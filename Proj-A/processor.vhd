-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------

-- processor.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a MIPS processor
--
--
-- NOTES:
-- 9/15/2019 by TJN::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.reg_array_type.all;
use work.full_adder_Nbit;

entity processor is
   port(i_CLK : in std_logic;
	rt : in std_logic_vector(4 downto 0);
	rd : in std_logic_vector(4 downto 0);	
	rw : in std_logic_vector(4 downto 0);
	nAddSub : in std_logic;
	ALUSrc : in std_logic;
	regWrite : in std_logic;
	immediate : in std_logic_vector(31 downto 0);
	o_Cout : out std_logic;
	o_D : out std_logic_vector(31 downto 0));
end processor;

architecture structural of processor is 

   component AddSub
	port(i_O1 : in std_logic_vector(31 downto 0);
	     i_O2 : in std_logic_vector(31 downto 0);
	     i_Cin : in std_logic;
	     i_Sel : in std_logic;
	     ALUSrc : in std_logic;
	     immediate : in std_logic_vector(31 downto 0);
	     o_Cout : out std_logic;
	     o_Sum : out std_logic_vector(31 downto 0));
   end component;

   component RegFile
	port(i_CLK : in std_logic;
	     i_RST : in std_logic;
	     rt : in std_logic_vector(4 downto 0);
	     rd : in std_logic_vector(4 downto 0);
	     rw : in std_logic_vector(4 downto 0);
	     WE : in std_logic;
	     Data : in std_logic_vector(31 downto 0);
	     o_rt : out std_logic_vector(31 downto 0);
	     o_rd : out std_logic_vector(31 downto 0));
   end component;

   signal s_Out1 : std_logic_vector(31 downto 0);
   signal s_Out2 : std_logic_vector(31 downto 0);

begin

   g_RegFile : RegFile
	port MAP(i_CLK => i_CLK,
		 i_RST => '0',
		 rt => rt,
	 	 rd => rd,
		 rw => rw,
		 WE => regWrite,
		 Data => immediate,
		 o_rt => s_Out1,
		 o_rd => s_Out2);

   g_AddSub : AddSub
	port MAP(i_O1 => s_Out1,
		 i_O2 => s_Out2,
		 i_Cin => '0',
		 i_Sel => nAddSub,
		 ALUSrc => ALUSrc,
		 immediate => immediate,
		 o_Cout => o_Cout,
		 o_Sum => o_D);

end structural;