-------------------------------------------------------------------------
-- Zachary Johnson and Trevor Nemes
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- MIPSprocessor.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the MIPS processor
-- for the memory module
-- 
--
--
-- NOTES:
-- 10/13/2019 by ZTJ&TJN::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity MIPSprocessor is
   port(immediate : std_logic_vector(15 downto 0);
	rt        : in std_logic_vector(4 downto 0);
	rd        : in std_logic_vector(4 downto 0);
	wt        : in std_logic_vector(4 downto 0);
	we1       : in std_logic;
	we2       : in std_logic;
	ctrl      : in std_logic;
	ALUSrc    : in std_logic;
	Load_Store : in std_logic;
	clk       : in std_logic;
	i_Op      : in std_logic_vector(2 downto 0);
	i_S       : in std_logic_vector(4 downto 0);
	i_AorL, i_RorL, i_ALUorShifter, i_RST : in std_logic;
	o_Cout, o_OF, zero : out std_logic);
end MIPSprocessor;

architecture structural of MIPSprocessor is

   component RegFile
	port(i_CLK : in std_logic;
	     i_RST : in std_logic;
	     rt    : in std_logic_vector(4 downto 0);
	     rd    : in std_logic_vector(4 downto 0);
	     rw    : in std_logic_vector(4 downto 0);
	     WE    : in std_logic;
	     Data  : in std_logic_vector(31 downto 0);
	     o_rt  : out std_logic_vector(31 downto 0);
	     o_rd  : out std_logic_vector(31 downto 0));
   end component;

   component extenderComponents
	generic(N : integer := 16);
        port(input  : in std_logic_vector(N-1 downto 0);
	     i_Ctrl : in std_logic;
	     output : out std_logic_vector(31 downto 0));
   end component;

   component ALU_32_bit_Barrel_Shifter
	port(i_A, i_B			: in std_logic_vector(31 downto 0);
             i_Op			: in std_logic_vector(2 downto 0);
	     i_S     			: in std_logic_vector(4 downto 0);
	     i_AorL 			: in std_logic;
	     i_RorL 			: in std_logic;
	     i_ALUorShifter 		: in std_logic;  --0 is ALU, 1 is Shifter
	     o_F			: out std_logic_vector(31 downto 0);
             o_Cout, o_OF, zero 	: out std_logic);
   end component;

   component mem
	generic (DATA_WIDTH : natural := 32;
		 ADDR_WIDTH : natural := 10);
	port(clk  : in std_logic;
	     addr : in std_logic_vector((ADDR_WIDTH-1) downto 0);
	     data : in std_logic_vector((DATA_WIDTH-1) downto 0);
	     we	  : in std_logic := '1';
	     q    : out std_logic_vector((DATA_WIDTH -1) downto 0));
   end component;

   component Nmux_dataflow
	generic(N : integer := 32);
	port(i_A : in std_logic_vector(N-1 downto 0);
       	     i_B : in std_logic_vector(N-1 downto 0);
             i_S : in std_logic;
             o_F : out std_logic_vector(N-1 downto 0));
   end component;

   signal RFtoALU  : std_logic_vector(31 downto 0);
   signal RFtoMUX  : std_logic_vector(31 downto 0);
   signal ECtoMUX  : std_logic_vector(31 downto 0);
   signal MUXtoALU : std_logic_vector(31 downto 0);
   signal ALUtoMUX : std_logic_vector(31 downto 0);
   signal MUXtoRF  : std_logic_vector(31 downto 0);
   signal DMout    : std_logic_vector(31 downto 0);

begin

   g_RegFile : RegFile
	port MAP(i_CLK => clk,
		 i_RST => i_RST,
		 rt => rt,
	 	 rd => rd,
		 rw => wt,
		 WE => we1,
		 Data => MUXtoRF,
		 o_rt => RFtoALU,
		 o_rd => RFtoMUX);

   g_extenderComponent : extenderComponents
	port MAP(input => immediate,
		 i_Ctrl => ctrl,
		 output => ECtoMUX);

   g_mux1 :  Nmux_dataflow
	port MAP(i_A => RFtoMUX,
		 i_B => ECtoMUX,
		 i_S => ALUSrc,
		 o_F => MUXtoALU);

   g_ALU : ALU_32_bit_Barrel_Shifter
	port MAP(i_A => RFtoALU,
		 i_B => MUXtoALU,
		 i_Op => i_Op,
		 i_S => i_S,
		 i_AorL => i_AorL,
		 i_RorL => i_RorL,
		 i_ALUorShifter => i_ALUorShifter,
		 o_F => ALUtoMUX,
		 o_Cout => o_Cout,
		 o_OF => o_OF,
		 zero => zero);

   g_mem : mem
	port MAP(clk => clk,
		 addr => ALUtoMUX(11 downto 2),
		 data => RFtoMUX,
		 we => we2,
		 q => DMout);

   g_mux2 :  Nmux_dataflow
	port MAP(i_A => ALUtoMUX,
		 i_B => DMout,
		 i_S => Load_Store,
		 o_F => MUXtoRF);

end structural;