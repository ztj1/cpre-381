library IEEE;
use IEEE.std_logic_1164.all;

package reg_array_type is
	type regArray is array(0 to 31) of std_logic_vector(31 downto 0);
end package reg_array_type;