-------------------------------------------------------------------------
-- Trevor Nemes
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- nbit_register.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a N-bit register
--
--
--
-- NOTES:
-- 9/11/2019 by TJN::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity nbit_register is
   generic(N: integer := 32);
   port(i_CLK        : in std_logic;  
        i_RST        : in std_logic;
        i_WE         : in std_logic;    
        i_D          : in std_logic_vector(N-1 downto 0);   
        o_Q          : out std_logic_vector(N-1 downto 0));   

end nbit_register;

architecture structure of nbit_register is

 component dff
   port(i_CLK        : in std_logic;   
       i_RST        : in std_logic;    
       i_WE         : in std_logic;     
       i_D          : in std_logic;     
       o_Q          : out std_logic); 
 end component;

begin

 G1: for i in 0 to 31 generate
   g_dff : dff
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_D(i),
	    o_Q => o_Q(i));
 end generate;

end structure;  
   