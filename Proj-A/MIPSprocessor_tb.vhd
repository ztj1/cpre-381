-------------------------------------------------------------------------
-- Zachary Johnson and Trevor Nemes
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- MIPSprocessor_tb.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the testbench for 
-- the memory module
-- 
--
--
-- NOTES:
-- 10/13/2019 by ZTJ&TJN::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity MIPSprocessor_tb is
   generic(gCLK_HPER : time := 50 ns);
end MIPSprocessor_tb;

architecture behavior of MIPSprocessor_tb is 

   constant cCLK_PER : time := gCLK_HPER * 2;

   component MIPSprocessor
	port(immediate  : std_logic_vector(15 downto 0);
	     rt         : in std_logic_vector(4 downto 0);
	     rd         : in std_logic_vector(4 downto 0);
	     wt         : in std_logic_vector(4 downto 0);
	     we1        : in std_logic;
	     we2        : in std_logic;
	     ctrl       : in std_logic;
	     ALUSrc     : in std_logic;
	     Load_Store : in std_logic;
	     clk        : in std_logic;
	     i_Op       : in std_logic_vector(2 downto 0);
	     i_S        : in std_logic_vector(4 downto 0);
	     i_AorL, i_RorL, i_ALUorShifter, i_RST : in std_logic;
	     o_Cout, o_OF, zero : out std_logic);
   end component;

   signal s_immediate : std_logic_vector(15 downto 0) := x"0000";
   signal s_rt : std_logic_vector(4 downto 0) := "00000";
   signal s_rd : std_logic_vector(4 downto 0) := "00000";  
   signal s_wt : std_logic_vector(4 downto 0) := "00000";
   signal s_we1 : std_logic := '0';
   signal s_we2 : std_logic := '0';
   signal s_ctrl : std_logic := '0';
   signal s_ALUSrc : std_logic := '0';
   signal s_LoadStore : std_logic := '0';
   signal s_CLK : std_logic;
   signal s_Op : std_logic_vector(2 downto 0);
   signal s_S : std_logic_vector(4 downto 0);
   signal s_AorL : std_logic;
   signal s_RorL : std_logic;
   signal s_ALUorShifter : std_logic;
   signal s_Cout : std_logic := '0';
   signal s_OF : std_logic;
   signal s_zero : std_logic;
   signal s_RST  : std_logic;

begin

   g_MIPSprocessor : MIPSprocessor
	port MAP(s_immediate, s_rt, s_rd, s_wt, s_we1, s_we2, s_ctrl, s_ALUSrc, s_LoadStore, s_CLK, s_Op, s_S, s_AorL, s_RorL, s_ALUorShifter, s_RST, s_Cout, s_OF, s_zero);

   p_clk : process
   begin
	s_CLK <= '0';
	wait for gCLK_HPER;
	s_CLK <= '1';
	wait for gCLK_HPER;
   end process;

   p_test : process
   begin

	
	s_RST <= '1';
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '0';
	s_we2 <= '0';
	wait for cCLK_PER;
	
	s_RST <= '0';
	s_immediate <= x"0001"; 	--Fill registers
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00000";
	s_rd <= "00010";
	s_wt <= "00001";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0010"; 
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00000";
	s_rd <= "00000";
	s_wt <= "00010";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0011"; 
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00000";
	s_rd <= "00000";
	s_wt <= "00011";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0100"; 
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00000";
	s_rd <= "00000";
	s_wt <= "00100";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0101"; 
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00000";
	s_rd <= "00000";
	s_wt <= "00101";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0110"; 
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00000";
	s_rd <= "00000";
	s_wt <= "00110";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0111"; 
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00000";
	s_rd <= "00000";
	s_wt <= "00111";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"1000"; 
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00000";
	s_rd <= "00000";
	s_wt <= "01000";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"1001"; 
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00000";
	s_rd <= "00000";
	s_wt <= "01001";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"1010"; 
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00000";
	s_rd <= "00000";
	s_wt <= "01010";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;



	s_immediate <= x"0000"; 	--sw $R2 -> mem[$1]
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '0';
	s_we2 <= '1';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "00010";
	s_ctrl <= '0';
	s_ALUSrc <= '0';
	s_LoadStore <= '0';
	wait for cCLK_PER;


	s_immediate <= x"0001"; 	--lw mem[$1] -> $2
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "00010";
	s_ctrl <= '0';
	s_ALUSrc <= '0';
	s_LoadStore <= '1';
	wait for cCLK_PER;

	s_immediate <= x"0000"; 	--add
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '0';
	s_ALUSrc <= '0';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0001"; 	--addi
	s_Op <= "000";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0000"; 	--sub
	s_Op <= "001";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '0';
	s_ALUSrc <= '0';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0000"; 	--subi
	s_Op <= "001";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0000"; 	--and
	s_Op <= "010";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '0';
	s_ALUSrc <= '0';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0001"; 	--andi
	s_Op <= "010";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0000"; 	--slt
	s_Op <= "011";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '0';
	s_ALUSrc <= '0';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0001"; 	--slti
	s_Op <= "011";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0000"; 	--or
	s_Op <= "100";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '0';
	s_ALUSrc <= '0';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0001"; 	--ori
	s_Op <= "100";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0000"; 	--xor
	s_Op <= "101";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '0';
	s_ALUSrc <= '0';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0001"; 	--xori
	s_Op <= "101";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0000"; 	--nand
	s_Op <= "110";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '0';
	s_ALUSrc <= '0';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0001"; 	--nandi
	s_Op <= "110";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0000"; 	--nor
	s_Op <= "111";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '0';
	s_ALUSrc <= '0';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0001"; 	--nori
	s_Op <= "111";
	s_S <= "00000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '0';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0001"; 	--sll
	s_Op <= "000";
	s_S <= "11000";
	s_AorL <= '1';
	s_RorL <= '1';
	s_ALUorShifter <= '1';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0001"; 	--srl
	s_Op <= "000";
	s_S <= "11000";
	s_AorL <= '1';
	s_RorL <= '0';
	s_ALUorShifter <= '1';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	s_immediate <= x"0001"; 	--sra
	s_Op <= "000";
	s_S <= "11000";
	s_AorL <= '0';
	s_RorL <= '0';
	s_ALUorShifter <= '1';
	s_we1 <= '1';
	s_we2 <= '0';
	s_rt <= "00001";
	s_rd <= "00000";
	s_wt <= "11001";
	s_ctrl <= '1';
	s_ALUSrc <= '1';
	s_LoadStore <= '0';
	wait for cCLK_PER;

	wait;

   end process;

end behavior;