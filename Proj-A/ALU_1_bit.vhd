-------------------------------------------------------------------------
-- Zachary Johnson and Trevor Nemes
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- ALU_1_bit.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: 1 bit full ALU
-- 
--
--
-- NOTES:
-- 10/1/2019 by ZTJ&TJN::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity ALU_1_bit is
port(i_A, i_B, i_AddSub	: in std_logic;
     i_S		: in std_logic_vector(2 downto 0);
     o_F, o_Cout	: out std_logic);
end ALU_1_bit;

architecture mixed of ALU_1_bit is

component mux_8_1 is
port(i_0, i_1, i_2, i_3, i_4, i_5, i_6, i_7	: in std_logic;
     i_S					: std_logic_vector(2 downto 0);
     o_F					: out std_logic);
end component;

component full_adder is
   port(i_A	: in std_logic;
	i_B	: in std_logic;
	i_Cin	: in std_logic;
	o_Cout	: out std_logic;
	o_Sum	: out std_logic);
end component;

component andg2 is
  port(i_A          : in std_logic;
       i_B          : in std_logic;
       o_F          : out std_logic);
end component;

component org2 is
  port(i_A          : in std_logic;
       i_B          : in std_logic;
       o_F          : out std_logic);
end component;

component invg is
  port(i_A          : in std_logic;
       o_F          : out std_logic);
end component;

component xorg2 is
  port(i_A          : in std_logic;
       i_B          : in std_logic;
       o_F          : out std_logic);
end component;

signal s_addsub, s_slt, s_and, s_or, s_xor, s_nand, s_nor, s_Cin, s_B, s_notA	: std_logic;

begin

adder : full_adder
port map(i_A => i_A,
	 i_B => s_B,
	 i_Cin => s_Cin,
	 o_Cout => o_Cout,
	 o_Sum => s_addsub);

xor1 : xorg2
port map(i_A => i_AddSub,
	 i_B => '0',
	 o_F => s_Cin);

xor2 : xorg2
port map(i_A => i_AddSub,
	 i_B => i_B,
	 o_F => s_B);

inv1 : invg
port map(i_A => i_A,
	 o_F => s_notA);

and1 : andg2
port map(i_A => s_notA,
	 i_B => i_B,
	 o_F => s_slt);

and2 : andg2
port map(i_A => i_A,
	 i_B => i_B,
	 o_F => s_and);

or1 : org2
port map(i_A => i_A,
	 i_B => i_B,
	 o_F => s_or);

xor3 : xorg2
port map(i_A => i_A,
	 i_B => i_B,
	 o_F => s_xor);

inv2 : invg
port map(i_A => s_and,
	 o_F => s_nand);

inv3 : invg
port map(i_A => s_or,
	 o_F => s_nor);

mux : mux_8_1
port map(s_addsub, s_slt, s_and, s_or, s_xor, s_nand, s_nor, '0', i_S, o_F);

end mixed;