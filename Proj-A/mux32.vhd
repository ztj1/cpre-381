-------------------------------------------------------------------------
-- Trevor Nemes
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- mux32.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a 32 to 1 mux
--
--
--
-- NOTES:
-- 9/11/2019 by TJN::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.reg_array_type.all;

entity mux32 is 
   port(i_A : in regArray;
	i_S : in std_logic_vector(4 downto 0);
	o_D : out std_logic_vector(31 downto 0));
end mux32;

architecture dataflow of mux32 is

begin

   o_D <= i_A(to_integer(unsigned(i_S)));

end dataflow;