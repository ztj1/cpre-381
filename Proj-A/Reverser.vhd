-------------------------------------------------------------------------
-- Trevor Nemes & Zach Johnson
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- Reverser.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a N-bit bit order 
-- reverser
--
--
-- NOTES:
-- 10/9/2019 by TJN & ZJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity Reverser is
   port(i_A : in std_logic_vector(31 downto 0);
	o_F : out std_logic_vector(31 downto 0));
end Reverser;

architecture dataflow of Reverser is

begin

G1: for i in 0 to 31 generate
   o_F(31 - i) <=  i_A(i);
end generate;

end dataflow;