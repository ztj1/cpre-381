-------------------------------------------------------------------------
-- Trevor Nemes
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- full_adder_Nbit.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a N-bit full adder
--
--
-- NOTES:
-- 9/4/2019 by TJN::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity full_adder_Nbit is
   generic(N: integer := 32);

   port(i_A : in std_logic_vector(N-1 downto 0);
	i_B : in std_logic_vector(N-1 downto 0);
	i_C : in std_logic;
	o_S : out std_logic_vector(N-1 downto 0);
	o_C : out std_logic);

end full_adder_Nbit;

architecture structure of full_adder_Nbit is

component full_adder is 
   port(i_oA : in std_logic;
	i_oB : in std_logic;
	i_Car : in std_logic;
	o_Sum : out std_logic;
	o_Car: out std_logic);
end component;

signal t_C : std_logic_vector(N downto 0);

begin
   t_C(0) <= i_C;

   G1: for i in 0 to N-1 generate
	g_full_adder : full_adder
	   port MAP(i_oA => i_A(i),
		    i_oB => i_B(i),
		    i_Car => t_C(i),
		    o_Sum => o_S(i),
		    o_Car => t_C(i+1));
   end generate;

	o_C <= t_C(N);

end structure;