-------------------------------------------------------------------------
-- Trevor Nemes & Zach Johnson
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- BarrelShifter.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a N-bit barrel
-- shifter
--
--
-- NOTES:
-- 10/9/2019 by TJN & ZTJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity BarrelShifter is
   port(i_A    : in std_logic_vector(31 downto 0);
	i_S    : in std_logic_vector(4 downto 0);
	i_AorL : in std_logic;
	i_RorL : in std_logic;
	o_F    : out std_logic_vector(31 downto 0));
end BarrelShifter;

architecture structural of BarrelShifter is

component Nmux_dataflow
   port(i_A : in std_logic_vector(31 downto 0);	--0
       	i_B : in std_logic_vector(31 downto 0);	--1
        i_S : in std_logic;
        o_F : out std_logic_vector(31 downto 0));
end component;

component Nbit_Shifter
   port(i_A : in std_logic_vector(31 downto 0);
	i_AorL : in std_logic;
	i_Bits : in integer range 0 to 16;
	o_F : out std_logic_vector(31 downto 0));
end component;

component Reverser
   port(i_A : in std_logic_vector(31 downto 0);
	o_F : out std_logic_vector(31 downto 0));
end component;

signal s_1to2 : std_logic_vector(31 downto 0);
signal s_2to4   : std_logic_vector(31 downto 0);
signal s_4to8   : std_logic_vector(31 downto 0);
signal s_8to16  : std_logic_vector(31 downto 0);
signal s_Shift1 : std_logic_vector(31 downto 0);
signal s_Shift2 : std_logic_vector(31 downto 0);
signal s_Shift4 : std_logic_vector(31 downto 0);
signal s_Shift8 : std_logic_vector(31 downto 0);
signal s_Shift16 : std_logic_vector(31 downto 0);
signal s_Rev1 : std_logic_vector(31 downto 0);
signal s_Rev2 : std_logic_vector(31 downto 0);
signal s_Input : std_logic_vector(31 downto 0);
signal s_Output : std_logic_vector(31 downto 0);

begin

rev1 : Reverser
   port Map(i_A => i_A,
	    o_F => s_Rev1);

RightORLeft1 : Nmux_dataflow
   port MAP(i_A => i_A,
	    i_B => s_Rev1,
	    i_S => i_RorL,
	    o_F => s_Input);

in1 : Nbit_Shifter
   port Map(i_A => s_Input,
	    i_AorL => i_AorL,
	    i_Bits => 1,
	    o_F => s_Shift1);

shift1 : Nmux_dataflow
   port MAP(i_A => s_Input,
	    i_B => s_Shift1,
	    i_S => i_S(4),
	    o_F => s_1to2);

in2 : Nbit_Shifter
   port Map(i_A => s_1to2,
	    i_AorL => i_AorL,
	    i_Bits => 2,
	    o_F => s_Shift2);

shift2 : Nmux_dataflow
   port MAP(i_A => s_1to2,
	    i_B => s_Shift2,
	    i_S => i_S(3),
	    o_F => s_2to4);

in4 : Nbit_Shifter
   port Map(i_A => s_2to4,
	    i_AorL => i_AorL,
	    i_Bits => 4,
	    o_F => s_Shift4);

shift4 : Nmux_dataflow
   port MAP(i_A => s_2to4,
	    i_B => s_Shift4,
	    i_S => i_S(2),
	    o_F => s_4to8);

in8 : Nbit_Shifter
   port Map(i_A => s_4to8,
	    i_AorL => i_AorL,
	    i_Bits => 8,
	    o_F => s_Shift8);

shift8 : Nmux_dataflow
   port MAP(i_A => s_4to8,
	    i_B => s_Shift8,
	    i_S => i_S(1),
	    o_F => s_8to16);

in16 : Nbit_Shifter
   port Map(i_A => s_8to16,
	    i_AorL => i_AorL,
	    i_Bits => 16,
	    o_F => s_Shift16);

shift16 : Nmux_dataflow
   port MAP(i_A => s_8to16,
	    i_B => s_Shift16,
	    i_S => i_S(0),
	    o_F => s_Output);

rev2 : Reverser
   port Map(i_A => s_Output,
	    o_F => s_Rev2);

RightORLeft2 : Nmux_dataflow
   port MAP(i_A => s_Output,
	    i_B => s_Rev2,
	    i_S => i_RorL,
	    o_F => o_F);

end structural;