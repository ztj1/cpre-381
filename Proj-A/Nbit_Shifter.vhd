-------------------------------------------------------------------------
-- Trevor Nemes & Zach Johnson
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- Nbit_Shifter.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a N-bit shifter
--
--
-- NOTES:
-- 10/9/2019 by TJN & ZJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity Nbit_Shifter is
   port(i_A : in std_logic_vector(31 downto 0);
	i_AorL : in std_logic;
	i_Bits : in integer range 0 to 16;
	o_F : out std_logic_vector(31 downto 0));
end Nbit_Shifter;

architecture behav of Nbit_Shifter is

signal s_ShiftValue : std_logic;

begin 

p1 : process (s_ShiftValue, i_AorL, i_A, i_Bits)
   variable i : integer range 0 to 32;
   variable j : integer range 0 to 64;
begin
   i := 0;
   j := i_Bits;

   if i_AorL = '0' then
      s_ShiftValue <= i_A(31); 
   elsif i_AorL = '1' then
      s_ShiftValue <= '0';
   end if;

   L1: while (i < 32) loop
	if i < 32 - i_Bits then
	   o_F(i) <= i_A(j);
	else
	   o_F(i) <= s_ShiftValue;
	end if;
	i := i + 1;
	j := j + 1;
   end loop L1;
end process p1;

end behav;