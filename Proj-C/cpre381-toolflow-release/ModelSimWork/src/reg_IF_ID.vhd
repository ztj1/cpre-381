-------------------------------------------------------------------------
-- Trevor Nemes & Zach Johnson
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- reg_IF_ID.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the IF/ID pipeline 
-- register
--
-- NOTES:
-- 11/18/2019 by TJN & ZJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity reg_IF_ID is
   generic(N: integer := 32);
   port(i_CLK        : in std_logic;  
        i_RST        : in std_logic;
        i_WE         : in std_logic;    
        i_PC	     : in std_logic_vector(N-1 downto 0);
	i_Inst	     : in std_logic_vector(N-1 downto 0);   
        o_PC         : out std_logic_vector(N-1 downto 0);
	o_Inst	     : out std_logic_vector(N-1 downto 0));   

end reg_IF_ID;

architecture structure of reg_IF_ID is

component nbit_register
generic(N: integer := 32);
   port(i_CLK        : in std_logic;  
        i_RST        : in std_logic;
        i_WE         : in std_logic;    
        i_D          : in std_logic_vector(N-1 downto 0);   
        o_Q          : out std_logic_vector(N-1 downto 0));
end component;

begin

PC_count : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_PC,
	     o_Q => o_PC);

Instruction : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_Inst,
	     o_Q => o_Inst);
	     
end structure;
