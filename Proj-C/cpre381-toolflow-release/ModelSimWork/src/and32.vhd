-------------------------------------------------------------------------
-- Trevor Nemes & Zach Johnson
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- and32.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a 32 bit AND gate
--
--
-- NOTES:
-- 10/8/2019 by TJN,ZJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity and32 is
   port(i_A : in std_logic_vector(31 downto 0);
	i_B : in std_logic_vector(31 downto 0);
	o_F : out std_logic_vector(31 downto 0));
end and32;

architecture dataflow of and32 is
begin

   G1: for i in 0 to 31 generate
	o_F(i) <= i_A(i) AND i_B(i);
   end generate;

end dataflow;