-------------------------------------------------------------------------
-- Zachary Johnson and Trevor Nemes
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- tb_pipeline.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: Testbench for the pipeline registers
-- 
--
--
-- NOTES:
-- 11/19/2019 by ZTJ&TJN::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_pipeline is
  generic(gCLK_HPER   : time := 10 ns;
          N           : integer := 32);  
end tb_pipeline;

architecture mixed of tb_pipeline is

component reg_IF_ID is
   generic(N: integer := 32);
   port(i_CLK        : in std_logic;  
        i_RST        : in std_logic;
        i_WE         : in std_logic;    
        i_PC	     : in std_logic_vector(N-1 downto 0);
	i_Inst	     : in std_logic_vector(N-1 downto 0);   
        o_PC         : out std_logic_vector(N-1 downto 0);
	o_Inst	     : out std_logic_vector(N-1 downto 0));  
end component;

component reg_ID_EX is
   generic(N: integer := 32);
   port(i_CLK       			: in std_logic;  
        i_RST        			: in std_logic;
        i_WE         			: in std_logic;
	i_upper_imm, i_RegDst, i_sltu, i_jal, i_MemtoReg, i_RegWrite, i_MemWrite, i_ALUSrc, i_AorL, i_RorL, i_ALUorShifter, i_ShiftVariable, i_Branch, i_jr  : in std_logic;
	i_Op				: in std_logic_vector(2 downto 0);      
	i_WrAddr, i_rd, i_rt, i_shamt	: in std_logic_vector(4 downto 0);
	i_rtD, i_rdD			: in std_logic_vector(N-1 downto 0);
	i_imm_ext, i_PC, i_v0, i_Inst			: in std_logic_vector(N-1 downto 0);
	o_upper_imm, o_RegDst, o_sltu, o_jal, o_MemtoReg, o_RegWrite, o_MemWrite, o_ALUSrc, o_AorL, o_RorL, o_ALUorShifter, o_ShiftVariable, o_Branch, o_jr  : out std_logic; 
	o_Op				: out std_logic_vector(2 downto 0);  
	o_WrAddr, o_rd, o_rt, o_shamt	: out std_logic_vector(4 downto 0);
	o_rtD, o_rdD			: out std_logic_vector(N-1 downto 0);
	o_imm_ext, o_PC, o_v0, o_Inst			: out std_logic_vector(N-1 downto 0));
end component;

component reg_EX_MEM is
   generic(N: integer := 32); 
   port(i_CLK       			: in std_logic;  
        i_RST        			: in std_logic;
        i_WE         			: in std_logic;
	i_upper_imm, i_sltu, i_jal, i_MemtoReg, i_RegWrite, i_MemWrite  : in std_logic;    
        i_ALUout			: in std_logic_vector(N-1 downto 0);
	i_ALUb	     			: in std_logic_vector(N-1 downto 0);  
	i_WrAddr			: in std_logic_vector(4 downto 0);
	i_sltuD, i_upper_immD, i_PC, i_v0, i_Inst	: in std_logic_vector(N-1 downto 0);
	o_upper_imm, o_sltu, o_jal, o_MemtoReg, o_RegWrite, o_MemWrite  : out std_logic; 
        o_ALUout        		: out std_logic_vector(N-1 downto 0);
	o_ALUb	     			: out std_logic_vector(N-1 downto 0);
	o_WrAddr			: out std_logic_vector(4 downto 0);
	o_sltuD, o_upper_immD, o_PC, o_v0, o_Inst	: out std_logic_vector(N-1 downto 0)); 
end component;

component reg_MEM_WB is
   port(i_CLK, i_RST, i_WE					: in std_logic;
	i_upper_imm, i_sltu, i_jal, i_MemtoReg, i_RegWrite	: in std_logic;
	i_DMEM, i_ALU, i_upper_immD, i_sltuD, i_PC, i_v0, i_Inst			: in std_logic_vector(31 downto 0);
	i_WrAddr						: in std_logic_vector(4 downto 0);
	o_upper_imm, o_sltu, o_jal, o_MemtoReg, o_RegWrite	: out std_logic;
	o_DMEM, o_ALU, o_upper_immD, o_sltuD, o_PC, o_v0, o_Inst			: out std_logic_vector(31 downto 0);
	o_WrAddr						: out std_logic_vector(4 downto 0));
end component;

signal CLK, flush_IF_ID, flush_ID_EX, flush_EX_MEM, flush_MEM_WB	: std_logic := '0';
signal stall_IF_ID, stall_ID_EX, stall_EX_MEM, stall_MEM_WB		: std_logic := '1';	

signal s_PC, s_Inst, s_rtD, s_rdD, s_imm_ext, s_ALUOut_MEM, s_ALUb, s_sltuD_MEM, s_upper_immD_MEM, s_DMEM, s_ALU_WB, s_upper_immD_WB, s_sltuD_WB 		: std_logic_vector(31 downto 0);
signal s_WrAddr_EX, s_WrAddr_MEM, s_rd, s_rt, s_shamt, s_WrAddr_WB												: std_logic_vector(4 downto 0);
signal s_Op																			: std_logic_vector(2 downto 0);
signal s_upper_imm_EX, s_upper_imm_MEM, s_sltu_EX, s_sltu_MEM, s_jal_EX, s_jal_MEM, s_MemtoReg_EX, s_MemtoReg_MEM, s_RegWrite_EX, s_RegWrite_MEM, s_MemWrite_EX, s_MemWrite_MEM, s_ALUSrc, s_AorL, s_RorL, s_ALUorShifter, s_ShiftVariable, s_upper_imm_WB, s_sltu_WB, s_jal_WB, s_MemtoReg_WB, s_RegWrite_WB	: std_logic;


begin

  P_CLK: process
  begin
    CLK <= '1';
    wait for gCLK_HPER;
    CLK <= '0';
    wait for gCLK_HPER;
  end process;

if_id : reg_IF_ID
  port map(i_CLK => CLK,
	   i_RST => flush_IF_ID,
	   i_WE => stall_IF_ID,
	   i_PC => x"00400000",
	   i_Inst => x"FFFFFFFF",
	   o_PC => s_PC,
	   o_Inst => s_Inst);

id_ex : reg_ID_EX
  port map(i_CLK => CLK,
	   i_RST => flush_ID_EX,
	   i_WE => stall_ID_EX,
	   i_upper_imm => '1', 
	   i_sltu => '1', 
	   i_jal => '1', 
	   i_MemtoReg => '1', 
	   i_RegWrite => '1', 
	   i_MemWrite => '1', 
	   i_ALUSrc => '0', 
	   i_AorL => '0', 
	   i_RorL => '0', 
	   i_ALUorShifter => '0', 
	   i_ShiftVariable => '0',
	   i_RegDst => '0',
	   i_Branch => '0',
 	   i_jr => '0',
	   i_PC => x"00030000",
	   i_v0 => x"00030000",
	   i_Inst => s_Inst,
	   i_Op => "000",     
	   i_WrAddr => s_Inst(15 downto 11), 
	   i_rd => s_Inst(20 downto 16), 
	   i_rt => s_Inst(25 downto 21), 
	   i_shamt => s_Inst(10 downto 6),
	   i_rtD => x"ff00ff00", 
	   i_rdD => x"00ff00ff",
	   i_imm_ext => x"0000ffff",
	   o_upper_imm => s_upper_imm_EX, 
	   o_sltu => s_sltu_EX, 
	   o_jal => s_jal_EX, 
	   o_MemtoReg => s_MemtoReg_EX, 
	   o_RegWrite => s_RegWrite_EX, 
	   o_MemWrite => s_MemWrite_EX, 
	   o_ALUSrc => s_ALUSrc, 
	   o_AorL => s_AorL, 
	   o_RorL => s_RorL, 
	   o_ALUorShifter => s_ALUorShifter, 
	   o_ShiftVariable => s_ShiftVariable,
	   o_RegDst => open,
	   o_Branch => open,
	   o_jr => open,
	   o_PC => open,
	   o_v0 => open,
	   o_Inst => open,
	   o_Op => s_Op,
	   o_WrAddr => s_WrAddr_EX, 
	   o_rd => s_rd, 
	   o_rt => s_rt, 
	   o_shamt => s_shamt,
	   o_rtD => s_rtD, 
	   o_rdD => s_rdD,
	   o_imm_ext => s_imm_ext);

ex_mem : reg_EX_MEM
  port map(i_CLK => CLK,
	   i_RST => flush_EX_MEM,
	   i_WE => stall_EX_MEM,
	   i_upper_imm => s_upper_imm_EX,
	   i_sltu => s_sltu_EX,
	   i_jal => s_jal_EX,
	   i_MemtoReg => s_MemtoReg_EX,
	   i_RegWrite => s_RegWrite_EX,
	   i_MemWrite => s_MemWrite_EX,
	   i_PC => x"00200000",
	   i_v0 => x"00020000",
	   i_Inst => s_Inst,
	   i_ALUOut => x"11111111",
	   i_ALUb => x"00001111",
	   i_WrAddr => s_WrAddr_EX,
	   i_sltuD => x"00000001",
	   i_upper_immD => x"10101010",
	   o_upper_imm => s_upper_imm_MEM,
	   o_sltu => s_sltu_MEM,
	   o_jal => s_jal_MEM,
	   o_MemtoReg => s_MemtoReg_MEM,
	   o_RegWrite => s_RegWrite_MEM,
	   o_MemWrite => s_MemWrite_MEM,
	   o_PC => open,
	   o_v0 => open,
	   o_Inst => open,
	   o_ALUOut => s_ALUOut_MEM,
	   o_ALUb => s_ALUb,
	   o_WrAddr => s_WrAddr_MEM,
	   o_sltuD => s_sltuD_MEM,
	   o_upper_immD => s_upper_immD_MEM);

mem_wb : reg_MEM_WB
  port map(i_CLK => CLK,
	   i_RST => flush_MEM_WB,
	   i_WE => stall_MEM_WB,
	   i_upper_imm => s_upper_imm_MEM,
	   i_sltu => s_sltu_MEM,
	   i_jal => s_jal_MEM,
	   i_MemtoReg => s_MemtoReg_MEM,
	   i_RegWrite => s_RegWrite_MEM,
	   i_PC => x"00100000",
	   i_v0 => x"00010000",
	   i_Inst => s_Inst,
	   i_DMEM => x"22222222",
	   i_ALU => s_ALUOut_MEM,
	   i_upper_immD => s_upper_immD_MEM,
	   i_sltuD => s_sltuD_MEM,
	   i_WrAddr => s_WrAddr_MEM,
	   o_upper_imm => s_upper_imm_WB,
	   o_sltu => s_sltu_WB,
	   o_jal => s_jal_WB,
	   o_MemtoReg => s_MemtoReg_WB,
	   o_RegWrite => s_RegWrite_WB,
	   o_PC => open,
	   o_v0 => open,
	   o_Inst => open,
	   o_DMEM => s_DMEM,
	   o_ALU => s_ALU_WB,
	   o_upper_immD => s_upper_immD_WB,
	   o_sltuD => s_sltuD_WB,
	   o_WrAddr => s_WrAddr_WB);


   p_test : process
   begin
	flush_IF_ID <= '1';
	flush_ID_EX <= '1';
	flush_EX_MEM <= '1';
	flush_MEM_WB <= '1';
	wait for gCLK_HPER;

	flush_IF_ID <= '0';
	flush_ID_EX <= '0';
	flush_EX_MEM <= '0';
	flush_MEM_WB <= '0';
	wait for gCLK_HPER;
	wait for gCLK_HPER;
	wait for gCLK_HPER;
	wait for gCLK_HPER;
	stall_IF_ID <= '0';
	stall_ID_EX <= '0';
	stall_EX_MEM <= '0';
	stall_MEM_WB <= '0';
	wait for gCLK_HPER;
	wait for gCLK_HPER;
	stall_IF_ID <= '1';
	stall_ID_EX <= '1';
	stall_EX_MEM <= '1';
	stall_MEM_WB <= '1';
	wait for gCLK_HPER;
	wait for gCLK_HPER;
	wait for gCLK_HPER;
	wait for gCLK_HPER;

	flush_IF_ID <= '1';
	flush_ID_EX <= '1';
	flush_EX_MEM <= '1';
	flush_MEM_WB <= '1';
	wait for gCLK_HPER;

	flush_IF_ID <= '0';
	flush_ID_EX <= '0';
	flush_EX_MEM <= '0';
	flush_MEM_WB <= '0';
	wait for gCLK_HPER;

   end process;



end mixed;