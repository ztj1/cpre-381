-------------------------------------------------------------------------
-- Trevor Nemes & Zach Johnson
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- MIPS_processor.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a skeleton of a Pipeline MIPS_Processor  
-- implementation.

-- 01/29/2019 by TN,ZJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity MIPS_processor is
  generic(N : integer := 32);
  port(iCLK            : in std_logic;
       iRST            : in std_logic;
       iInstLd         : in std_logic;
       iInstAddr       : in std_logic_vector(N-1 downto 0);
       iInstExt        : in std_logic_vector(N-1 downto 0);
       oALUOut         : out std_logic_vector(N-1 downto 0)); -- TODO: Hook this up to the output of the ALU. It is important for synthesis that you have this output that can effectively be impacted by all other components so they are not optimized away.
end  MIPS_processor;

architecture structure of MIPS_processor is

  -- Required data memory signals
  signal s_DMemWr       : std_logic; -- TODO: use this signal as the final active high data memory write enable signal
  signal s_DMemAddr     : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory address input
  signal s_DMemData     : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory data input
  signal s_DMemOut      : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the data memory output
 
  -- Required register file signals 
  signal s_RegWr        : std_logic; -- TODO: use this signal as the final active high write enable input to the register file
  signal s_RegWrAddr    : std_logic_vector(4 downto 0); -- TODO: use this signal as the final destination register address input
  signal s_RegWrData    : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory data input

  -- Required instruction memory signals
  signal s_IMemAddr     : std_logic_vector(N-1 downto 0); -- Do not assign this signal, assign to s_NextInstAddr instead
  signal s_NextInstAddr : std_logic_vector(N-1 downto 0); -- TODO: use this signal as your intended final instruction memory address input.
  signal s_Inst         : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the instruction signal 

  -- Required halt signal -- for simulation
  signal v0             : std_logic_vector(N-1 downto 0); -- TODO: should be assigned to the output of register 2, used to implement the halt SYSCALL
  signal s_Halt         : std_logic;  -- TODO: this signal indicates to the simulation that intended program execution has completed. This case happens when the syscall instruction is observed and the V0 register is at 0x0000000A. This signal is active high and should only be asserted after the last register and memory writes before the syscall are guaranteed to be completed.

  component mem is
    generic(ADDR_WIDTH : integer;
            DATA_WIDTH : integer);
    port(
          clk          : in std_logic;
          addr         : in std_logic_vector((ADDR_WIDTH-1) downto 0);
          data         : in std_logic_vector((DATA_WIDTH-1) downto 0);
          we           : in std_logic := '1';
          q            : out std_logic_vector((DATA_WIDTH -1) downto 0));
    end component;

  component RegFile is
    generic(N : integer := 32);
    port(i_CLK : in std_logic;
	 i_RST : in std_logic;
	 rt : in std_logic_vector(4 downto 0);
	 rd : in std_logic_vector(4 downto 0);
	 rw : in std_logic_vector(4 downto 0);
	 WE : in std_logic;
	 Data : in std_logic_vector(N-1 downto 0);
	 o_2  : out std_logic_vector(N-1 downto 0);
	 o_rt : out std_logic_vector(N-1 downto 0);
	 o_rd : out std_logic_vector(N-1 downto 0));
  end component;
  
  component Nmux_dataflow is
	generic(N : integer := 32);
	port(i_A : in std_logic_vector(N-1 downto 0);	--0
       	     i_B : in std_logic_vector(N-1 downto 0);	--1
             i_S : in std_logic;
             o_F : out std_logic_vector(N-1 downto 0));
  end component;

  component ALU_32_bit_Barrel_Shifter is
    port(i_A, i_B		: in std_logic_vector(31 downto 0);
         i_Op			: in std_logic_vector(2 downto 0);
	 i_S     		: in std_logic_vector(4 downto 0);
	 i_AorL 			: in std_logic;
	 i_RorL 			: in std_logic;
	 i_ALUorShifter 		: in std_logic;  --0 is ALU, 1 is Shifter
	 o_F			: out std_logic_vector(31 downto 0);
         o_Cout, o_OF, zero 	: out std_logic);
  end component;

  component n_bit_full_adder is
    generic(N : integer := 32);
    port(i_A	: in std_logic_vector(N-1 downto 0);
	 i_B	: in std_logic_vector(N-1 downto 0);
	 i_Cin	: in std_logic;
	 o_Cout	: out std_logic;
	 o_Cout_1: out std_logic;
	 o_Sum	: out std_logic_vector(N-1 downto 0));
  end component;

  component nbit_register is
    generic(N: integer := 32);
    port(i_CLK        : in std_logic;  
         i_RST        : in std_logic;
         i_WE         : in std_logic;    
         i_D          : in std_logic_vector(N-1 downto 0);   
         o_Q          : out std_logic_vector(N-1 downto 0));
  end component;

  component extenderComponents is
    generic(N: integer := 16);
    port(input 		: in std_logic_vector(15 downto 0);
	 i_Ctrl 	: in std_logic;
	 output 	: out std_logic_vector(31 downto 0));
  end component;

  component BarrelShifter
     generic(N : integer := 32);
     port(i_A    : in std_logic_vector(N-1 downto 0);
	  i_S    : in std_logic_vector(4 downto 0);
	  i_AorL : in std_logic;
	  i_RorL : in std_logic;
	  o_F    : out std_logic_vector(N-1 downto 0));
  end component;

  component ControlUnit is
    port(op_Code        									: in std_logic_vector(5 downto 0);
	 Funct	       										: in std_logic_vector(5 downto 0);
	 o_Op          										: out std_logic_vector(2 downto 0);
	 RegDst, o_beq, o_bne, Jump, jr, MemtoReg, MemWrite, ALUSrc, RegWrite 			: out std_logic;
	 o_Signed, o_AorL, o_RorL, o_ALUorShifter, o_ShiftVariable, o_upper_imm, o_sltu 	: out std_logic);
  end component;

  component PC_register is
    generic(N: integer := 32);
    port(i_CLK        : in std_logic;  
         i_RST        : in std_logic;
         i_WE         : in std_logic;    
         i_D          : in std_logic_vector(N-1 downto 0);   
         o_Q          : out std_logic_vector(N-1 downto 0));
  end component;

  component reg_IF_ID is
   generic(N: integer := 32);
   port(i_CLK        : in std_logic;  
        i_RST        : in std_logic;
        i_WE         : in std_logic;    
        i_PC	     : in std_logic_vector(N-1 downto 0);
	i_Inst	     : in std_logic_vector(N-1 downto 0);   
        o_PC         : out std_logic_vector(N-1 downto 0);
	o_Inst	     : out std_logic_vector(N-1 downto 0));   
  end component;

  component reg_ID_EX is
   generic(N: integer := 32);
   port(i_CLK       			: in std_logic;  
        i_RST        			: in std_logic;
        i_WE         			: in std_logic;
	i_upper_imm, i_RegDst, i_sltu, i_jal, i_MemtoReg, i_RegWrite, i_MemWrite, i_ALUSrc, i_AorL, i_RorL, i_ALUorShifter, i_ShiftVariable, i_Branch, i_jr  : in std_logic;
	i_Op				: in std_logic_vector(2 downto 0);      
	i_WrAddr, i_rd, i_rt, i_shamt	: in std_logic_vector(4 downto 0);
	i_rtD, i_rdD			: in std_logic_vector(N-1 downto 0);
	i_imm_ext, i_PC, i_v0, i_Inst			: in std_logic_vector(N-1 downto 0);
	o_upper_imm, o_RegDst, o_sltu, o_jal, o_MemtoReg, o_RegWrite, o_MemWrite, o_ALUSrc, o_AorL, o_RorL, o_ALUorShifter, o_ShiftVariable, o_Branch, o_jr  : out std_logic; 
	o_Op				: out std_logic_vector(2 downto 0);  
	o_WrAddr, o_rd, o_rt, o_shamt	: out std_logic_vector(4 downto 0);
	o_rtD, o_rdD			: out std_logic_vector(N-1 downto 0);
	o_imm_ext, o_PC, o_v0, o_Inst			: out std_logic_vector(N-1 downto 0));   
  end component;

  component reg_EX_MEM is
   generic(N: integer := 32);
   port(i_CLK       		: in std_logic;  
        i_RST        		: in std_logic;
        i_WE         		: in std_logic;
	i_upper_imm, i_sltu, i_jal, i_MemtoReg, i_RegWrite, i_MemWrite  : in std_logic;    
        i_ALUout		: in std_logic_vector(N-1 downto 0);
	i_ALUb	     		: in std_logic_vector(N-1 downto 0);  
	i_WrAddr		: in std_logic_vector(4 downto 0);
	i_sltuD, i_upper_immD, i_PC, i_v0, i_Inst	: in std_logic_vector(N-1 downto 0);
	o_upper_imm, o_sltu, o_jal, o_MemtoReg, o_RegWrite, o_MemWrite  : out std_logic; 
        o_ALUout        	: out std_logic_vector(N-1 downto 0);
	o_ALUb	     		: out std_logic_vector(N-1 downto 0);
	o_WrAddr		: out std_logic_vector(4 downto 0);
	o_sltuD, o_upper_immD, o_PC, o_v0, o_Inst	: out std_logic_vector(N-1 downto 0));   
  end component;

  component reg_MEM_WB is
   port(i_CLK, i_RST, i_WE						: in std_logic;
	i_upper_imm, i_sltu, i_jal, i_MemtoReg, i_RegWrite		: in std_logic;
	i_DMEM, i_ALU, i_upper_immD, i_sltuD, i_PC, i_v0, i_Inst	: in std_logic_vector(31 downto 0);
	i_WrAddr							: in std_logic_vector(4 downto 0);
	o_upper_imm, o_sltu, o_jal, o_MemtoReg, o_RegWrite		: out std_logic;
	o_DMEM, o_ALU, o_upper_immD, o_sltuD, o_PC, o_v0, o_Inst	: out std_logic_vector(31 downto 0);
	o_WrAddr							: out std_logic_vector(4 downto 0));
  end component;

  component fwd_unit is
   generic(N : integer := 32);
  port(EX_MEM_RegWrite, MEM_WB_RegWrite, jr, beq, bne, sltu, upper_imm				: in std_logic;
       ID_EX_rd, ID_EX_rt, EX_MEM_WrAddr, MEM_WB_WrAddr, IF_ID_rt, IF_ID_rd, EX_RegDst		: in std_logic_vector(4 downto 0);
       fwd_mux_C  						: out std_logic;
       fwd_mux_A, fwd_mux_B, fwd_mux_D, fwd_mux_E				: out std_logic_vector(1 downto 0));
  end component;

  component hazard_unit is
   generic(N : integer := 32);
  port(branch, jump, ID_EX_Branch					: in std_logic;
       ID_EX_MemtoReg, jr, ID_EX_Jump, ID_EX_jr, EX_MEM_MemtoReg	: in std_logic;
       IF_ID_rd, IF_ID_rt, EX_MEM_RegDst_muxD, EX_MEM_WrAddr		: in std_logic_vector(4 downto 0);
       ID_EX_Inst, EX_MEM_Inst						: in std_logic_vector(31 downto 0);
       IF_ID_flush, IF_ID_stall, ID_EX_stall, PC_stall, ID_EX_flush	: out std_logic);
  end component;

  component Nmux_4to1 is
	generic(N : integer := 32);
	port(i_A : in std_logic_vector(N-1 downto 0);	--00
       	     i_B : in std_logic_vector(N-1 downto 0);	--01
	     i_C : in std_logic_vector(N-1 downto 0);   --10
	     i_D : in std_logic_vector(N-1 downto 0);   --11
             i_S : in std_logic_vector(1 downto 0);
             o_F : out std_logic_vector(N-1 downto 0));
  end component;

  signal s_ALUSrcD, s_imm_ext, s_rt, s_rd, s_ALUOut, s_upper_immD, s_jump_addr_top, s_PCOut, s_MemtoRegD, s_upper_imm_muxD, s_sltuD, s_jump_addr, s_jr_muxD, s_jump_muxD, s_sltu_muxD, s_branch_muxD, s_branch_add_out, s_imm_sl2, s_fwd_data_spec	: std_logic_vector(31 downto 0);
  signal s_IF_ID_PC, s_IF_ID_Inst, s_ID_EX_rtD, s_ID_EX_rdD, s_ID_EX_imm_ext, s_ID_EX_PC, s_EX_MEM_sltuD, s_EX_MEM_upper_immD, s_EX_MEM_PC, s_MEM_WB_DMemOut, s_MEM_WB_ALUOut, s_MEM_WB_upper_immD, s_MEM_WB_sltuD, s_MEM_WB_PC, s_ALU_mux_AD, s_ALU_mux_BD, s_ex_fwd_data_spec		: std_logic_vector(31 downto 0);
  signal s_IF_ID_v0, s_ID_EX_v0, s_EX_MEM_v0, s_PC_muxD, s_ID_EX_Inst, s_EX_MEM_Inst, s_Inst_in, s_rtD, s_rdD	: std_logic_vector(31 downto 0);
  signal s_jump_temp, s_jump_addr_bottom																				: std_logic_vector(27 downto 0);
  signal s_ShiftBits, s_RegDst_muxD, s_EX_MEM_RegDst_muxD																					: std_logic_vector(4 downto 0);
  signal s_ID_EX_rt, s_ID_EX_rd, s_ID_EX_shamt, s_EX_MEM_WrAddr, s_ID_EX_WrAddr			: std_logic_vector(4 downto 0);
  signal s_ALUOp																					: std_logic_vector(2 downto 0);
  signal s_fwd_mux_A, s_fwd_mux_B, s_fwd_mux_D, s_fwd_mux_E	: std_logic_vector(1 downto 0);
  signal s_ID_EX_ALUOp				: std_logic_vector(2 downto 0);
  signal s_RegDst, s_ALUSrc, s_Signed, s_AorL, s_RorL, s_ALUorShifter, s_zero, s_MemtoReg, s_Jump, s_ShiftVariable, s_upper_imm, s_sltu, s_bne, s_beq, s_Branch, s_jr, s_IF_ID_flush, s_IF_ID_stall, s_ID_EX_stall, s_PC_stall, s_IF_ID_reset, s_ID_EX_reset			: std_logic;
  signal s_MemWrite, s_ID_EX_upper_imm, s_ID_EX_RegDst, s_ID_EX_sltu, s_ID_EX_Jump, s_ID_EX_MemtoReg, s_ID_EX_RegWrite, s_ID_EX_MemWrite, s_ID_EX_ALUSrc, s_ID_EX_AorL, s_ID_EX_RorL, s_ID_EX_ALUorShifter, s_ID_EX_ShiftVariable, s_ID_EX_jr	: std_logic;
  signal s_EX_MEM_upper_imm, s_EX_MEM_sltu, s_EX_MEM_Jump, s_EX_MEM_MemtoReg, s_EX_MEM_RegWrite, s_MEM_WB_upper_imm, s_MEM_WB_sltu, s_MEM_WB_Jump, s_MEM_WB_MemtoReg, s_IF_ID_RegWr, s_PC_sel, s_ID_EX_Branch, s_fwd_mux_C, s_ID_EX_flush	: std_logic;

begin

  -- This is required to be your final input to your instruction memory. This provides a feasible method to externally load the memory module which means that the synthesis tool must assume it knows nothing about the values stored in the instruction memory. If this is not included, much, if not all of the design is optimized out because the synthesis tool will believe the memory to be all zeros.
  with iInstLd select
    s_IMemAddr <= s_NextInstAddr when '0',
      iInstAddr when others;


  IMem: mem
    generic map(ADDR_WIDTH => 10,
                DATA_WIDTH => N)
    port map(clk  => iCLK,
             addr => s_IMemAddr(11 downto 2),
             data => iInstExt,
             we   => iInstLd,
             q    => s_Inst_in);
  
  DMem: mem
    generic map(ADDR_WIDTH => 10,
                DATA_WIDTH => N)
    port map(clk  => iCLK,
             addr => s_DMemAddr(11 downto 2),
             data => s_DMemData,
             we   => s_DMemWr,
             q    => s_DMemOut);

  s_Halt <='1' when (s_Inst(31 downto 26) = "000000") and (s_Inst(5 downto 0) = "001100") and (v0 = "00000000000000000000000000001010") else '0';

  -- Implement the rest of your processor below this comment! 

  --IF stage
  PC : PC_register
    generic map(N => 32)
    port map(i_CLK => iCLK,
	     i_RST => iRST,
	     i_WE => s_PC_stall,
	     i_D => s_PC_muxD,
	     o_Q => s_PCOut);
  
  PC_increment : n_bit_full_adder
    generic map(N => 32)
    port map(i_A => s_PCOut,
	 i_B => x"00000004",
	 i_Cin => '0',
	 o_Cout => open,
	 o_Cout_1 => open,
	 o_Sum => s_jump_addr_top);

  s_NextInstAddr <= s_PCOut;

  s_PC_sel <= s_Branch or s_Jump or s_jr;

  PC_mux : Nmux_dataflow
    generic map(N => 32)
    port map(i_A => s_jump_addr_top,
	     i_B => s_jr_muxD,
	     i_S => s_PC_sel,
	     o_F => s_PC_muxD);

  --IF/ID Register
  IF_ID : reg_IF_ID
   generic map(N => 32)
   port map(i_CLK => iCLK,
        i_RST => s_IF_ID_reset,
        i_WE => s_IF_ID_stall,   
        i_PC => s_jump_addr_top,
	i_Inst => s_Inst_in,
        o_PC => s_IF_ID_PC,
	o_Inst => s_IF_ID_Inst);

  IF_ID_flush : process (iRST, s_IF_ID_flush, iCLK)
  begin
    if (iRST = '1') then
      s_IF_ID_reset <= '1';
    else
      s_IF_ID_reset <= s_IF_ID_flush;
    end if;
  end process;


  --ID Stage
  reg : RegFile
    generic map(N => 32)
    port map(i_CLK => iCLK,
	     i_RST => iRST,
	     rt => s_IF_ID_Inst(25 downto 21),
	     rd => s_IF_ID_Inst(20 downto 16),
	     rw => s_RegWrAddr,
	     WE => s_RegWr,
	     Data => s_RegWrData,
	     o_2 => v0,
	     o_rt => s_rt,
	     o_rd => s_rd);

  fwd_mux_rtD : Nmux_4to1
    generic map(N => 32)
    port map(i_A => s_rt,
	     i_B => s_RegWrData,
	     i_C => s_ex_fwd_data_spec,
	     i_D => s_fwd_data_spec,
	     i_S => s_fwd_mux_D,
	     o_F => s_rtD);

  fwd_mux_rdD : Nmux_4to1
    generic map(N => 32)
    port map(i_A => s_rd,
	     i_B => s_RegWrData,
	     i_C => s_ex_fwd_data_spec,
	     i_D => s_fwd_data_spec,
	     i_S => s_fwd_mux_E,
	     o_F => s_rdD);

  jal_read_mux : Nmux_dataflow
    generic map(N => 5)
    port map(i_A => s_RegDst_muxD,
	     i_B => "11111",
	     i_S => s_MEM_WB_Jump,
	     o_F => s_RegWrAddr);

  control : ControlUnit
    port map(op_Code => s_IF_ID_Inst(31 downto 26),
	     Funct => s_IF_ID_Inst(5 downto 0),
	     o_Op => s_ALUOp,
	     RegDst => s_RegDst,
	     Jump => s_Jump,
	     MemtoReg => s_MemtoReg,
	     MemWrite => s_MemWrite,
	     ALUSrc => s_ALUSrc,
	     RegWrite => s_IF_ID_RegWr,
	     o_Signed => s_Signed,
	     o_AorL => s_AorL,
	     o_RorL => s_RorL,
	     o_ALUorShifter => s_ALUorShifter,
	     o_ShiftVariable => s_ShiftVariable,
	     o_upper_imm => s_upper_imm,
	     o_sltu => s_sltu,
	     o_bne => s_bne,
	     o_beq => s_beq,
	     jr => s_jr);

  imm_extender : extenderComponents
    generic map(N => 16)
    port map(input => s_IF_ID_Inst(15 downto 0),
	     i_Ctrl => s_Signed,
	     output => s_imm_ext);

  s_jump_temp(27 downto 26) <= "00";
  s_jump_temp(25 downto 0) <= s_IF_ID_Inst(25 downto 0);

  jump_addr_shift : BarrelShifter
    generic map(N => 28)
    port map(i_A => s_jump_temp,
	     i_S => "00010",
	     i_AorL => '1',
	     i_RorL => '1',
	     o_F => s_jump_addr_bottom);

  s_jump_addr(31 downto 28) <= s_IF_ID_PC(31 downto 28);
  s_jump_addr(27 downto 0) <= s_jump_addr_bottom;

  jump_mux : Nmux_dataflow
    port map(i_A => s_branch_muxD,
	     i_B => s_jump_addr,
	     i_S => s_Jump,
	     o_F => s_jump_muxD);

  jr_mux : Nmux_dataflow
    port map(i_A => s_jump_muxD,
	     i_B => s_rtD,
	     i_S => s_jr,
	     o_F => s_jr_muxD);

  branch_shift : BarrelShifter
    generic map(N => 32)
    port map(i_A => s_imm_ext,
	     i_S => "00010",
	     i_AorL => '1',
	     i_RorL => '1',
	     o_F => s_imm_sl2);

  branch_add : n_bit_full_adder
    port map(i_A => s_IF_ID_PC,
	     i_B => s_imm_sl2,
	     i_Cin => '0',
	     o_Cout => open,
	     o_Cout_1 => open,
	     o_Sum => s_branch_add_out);

  branch_mux : Nmux_dataflow
    port map(i_A => s_IF_ID_PC,
	     i_B => s_branch_add_out,
	     i_S => s_Branch,
	     o_F => s_branch_muxD);

  hazard : hazard_unit
    port map(branch => s_Branch,
	     jump => s_Jump,
	     ID_EX_Branch => s_ID_EX_Branch,
       	     ID_EX_MemtoReg => s_ID_EX_MemtoReg,
	     IF_ID_rd => s_IF_ID_Inst(20 downto 16),
	     IF_ID_rt => s_IF_ID_Inst(25 downto 21),
	     jr => s_jr,
	     EX_MEM_WrAddr => s_EX_MEM_WrAddr,
	     EX_MEM_MemtoReg => s_EX_MEM_MemtoReg,
	     ID_EX_Jump => s_ID_EX_Jump,
	     ID_EX_jr => s_ID_EX_jr,
	     ID_EX_Inst => s_ID_EX_Inst,
	     EX_MEM_Inst => s_EX_MEM_Inst,
	     EX_MEM_RegDst_muxD => s_EX_MEM_RegDst_muxD,
       	     IF_ID_flush => s_IF_ID_flush,
	     IF_ID_stall => s_IF_ID_stall,
	     ID_EX_stall => s_ID_EX_stall,
	     ID_EX_flush => s_ID_EX_flush,
	     PC_stall => s_PC_stall);

  fwd : fwd_unit
    port map(EX_MEM_RegWrite => s_EX_MEM_RegWrite,
	     MEM_WB_RegWrite => s_RegWr,
       	     ID_EX_rd => s_ID_EX_rd,
	     ID_EX_rt => s_ID_EX_rt,
	     IF_ID_rt => s_IF_ID_Inst(25 downto 21),
	     IF_ID_rd => s_IF_ID_Inst(20 downto 16),
	     jr => s_jr,
	     beq => s_beq,
	     bne => s_bne,
	     sltu => s_sltu,
	     upper_imm => s_upper_imm,
	     EX_RegDst => s_EX_MEM_RegDst_muxD,
	     EX_MEM_WrAddr => s_EX_MEM_WrAddr,
	     MEM_WB_WrAddr => s_RegWrAddr,
	     fwd_mux_C => s_fwd_mux_C,
       	     fwd_mux_A => s_fwd_mux_A,
	     fwd_mux_B => s_fwd_mux_B,
	     fwd_mux_D => s_fwd_mux_D,
	     fwd_mux_E => s_fwd_mux_E);

  --equals logic
  equals : ALU_32_bit_Barrel_Shifter
  port map(i_A => s_rtD,
	   i_B => s_rdD,
	   i_Op => "001",
	   i_S => "00000",
	   i_AorL => '0',
	   i_RorL => '0',
	   i_ALUorShifter => '0',
	   o_F => open,
	   o_Cout => open,
	   o_OF => open,
	   zero => s_zero);

  branch : process (s_beq, s_bne, s_zero)
  begin
    if ((s_zero = '1') and (s_beq = '1')) then
      s_Branch <= '1';
    elsif ((s_zero = '0') and (s_bne = '1')) then
      s_Branch <= '1';
    else
      s_Branch <= '0';
    end if;
  end process;

  --ID/EX Register
  ID_EX : reg_ID_EX
   generic map(N => 32)
   port map(i_CLK => iCLK,
        i_RST => s_ID_EX_reset,
        i_WE => s_ID_EX_stall,
	i_upper_imm => s_upper_imm,
	i_RegDst => s_RegDst, 
	i_sltu => s_sltu, 
	i_jal => s_Jump, 
	i_MemtoReg => s_MemtoReg, 
	i_RegWrite => s_IF_ID_RegWr, 
	i_MemWrite => s_MemWrite, 
	i_ALUSrc => s_ALUSrc, 
	i_AorL => s_AorL, 
	i_RorL => s_RorL, 
	i_ALUorShifter => s_ALUorShifter, 
	i_ShiftVariable => s_ShiftVariable,
	i_Branch => s_Branch,
	i_Op => s_ALUOp,    
	i_WrAddr => s_IF_ID_Inst(15 downto 11), 
	i_rt => s_IF_ID_Inst(25 downto 21),
	i_rd => s_IF_ID_Inst(20 downto 16),
	i_shamt => s_IF_ID_Inst(10 downto 6),
	i_rtD => s_rtD, 
	i_rdD => s_rdD,
	i_imm_ext => s_imm_ext, 
	i_PC => s_IF_ID_PC,
	i_v0 => s_IF_ID_v0,
	i_Inst => s_IF_ID_Inst,
	i_jr => s_jr,
	o_upper_imm => s_ID_EX_upper_imm, 
	o_RegDst => s_ID_EX_RegDst, 
	o_sltu => s_ID_EX_sltu, 
	o_jal => s_ID_EX_Jump, 
	o_MemtoReg => s_ID_EX_MemtoReg, 
	o_RegWrite => s_ID_EX_RegWrite, 
	o_MemWrite => s_ID_EX_MemWrite, 
	o_ALUSrc => s_ID_EX_ALUSrc, 
	o_AorL => s_ID_EX_AorL, 
	o_RorL => s_ID_EX_RorL, 
	o_ALUorShifter => s_ID_EX_ALUorShifter, 
	o_ShiftVariable => s_ID_EX_ShiftVariable,
	o_Branch => s_ID_EX_Branch,
	o_Op => s_ID_EX_ALUOp, 
	o_WrAddr => s_ID_EX_WrAddr, 
	o_rd => s_ID_EX_rd, 
	o_rt => s_ID_EX_rt, 
	o_shamt => s_ID_EX_shamt,
	o_rtD => s_ID_EX_rtD, 
	o_rdD => s_ID_EX_rdD,
	o_imm_ext => s_ID_EX_imm_ext, 
	o_PC => s_ID_EX_PC,
	o_v0 => s_ID_EX_v0,
	o_Inst => s_ID_EX_Inst,
	o_jr => s_ID_EX_jr);  

  ID_EX_flush : process (iRST, s_ID_EX_flush, iCLK)
  begin
    if (iRST = '1') then
      s_ID_EX_reset <= '1';
    else
      s_ID_EX_reset <= s_ID_EX_flush;
    end if;
  end process;

  --EX Stage
  RegDst_mux : Nmux_dataflow
    generic map(N => 5)
    port map(i_A => s_ID_EX_rd,
	     i_B => s_ID_EX_WrAddr,
	     i_S => s_ID_EX_RegDst,
	     o_F => s_EX_MEM_RegDst_muxD);

  ALUSrc_mux : Nmux_dataflow
    generic map(N => 32)
    port map(i_A => s_ALU_mux_BD,
	     i_B => s_ID_EX_imm_ext,
	     i_S => s_ID_EX_ALUSrc,
	     o_F => s_ALUSrcD);

  ShiftVariable_mux : Nmux_dataflow
    generic map(N => 5)
    port map(i_A => s_ID_EX_shamt,
	     i_B => s_ALU_mux_AD(4 downto 0),
	     i_S => s_ID_EX_ShiftVariable,
	     o_F => s_ShiftBits);

  ALU_mux_A : Nmux_4to1
    generic map(N => 32)
    port map(i_A => s_ID_EX_rtD,
	     i_B => s_RegWrData,
	     i_C => s_fwd_data_spec,
	     i_D => x"00000000",
	     i_S => s_fwd_mux_A,
	     o_F => s_ALU_mux_AD);

  ALU_mux_B : Nmux_4to1
    generic map(N => 32)
    port map(i_A => s_ID_EX_rdD,
	     i_B => s_RegWrData,
	     i_C => s_fwd_data_spec,
	     i_D => x"00000000",
	     i_S => s_fwd_mux_B,
	     o_F => s_ALU_mux_BD);

  

  ALU : ALU_32_bit_Barrel_Shifter
    port map(i_A => s_ALU_mux_AD,
	     i_B => s_ALUSrcD,
	     i_Op => s_ID_EX_ALUOp,
	     i_S => s_ShiftBits,
	     i_AorL => s_ID_EX_AorL,
	     i_RorL => s_ID_EX_RorL,
	     i_ALUorShifter => s_ID_EX_ALUorShifter,
	     o_F => s_ALUOut,
	     o_Cout => open,
	     o_OF => open,
	     zero => open);

  oALUOut <= s_ALUOut;

  imm_shift : BarrelShifter
    port map(i_A => s_ID_EX_imm_ext,
	     i_S => "10000",
	     i_AorL => '1',
	     i_RorL => '1',
	     o_F => s_upper_immD);

  ex_fwd_sel : process (s_ID_EX_sltu, s_ID_EX_upper_imm, s_sltuD, s_upper_immD, s_ALUOut, s_ID_EX_PC, s_ID_EX_Jump)
  begin
    if (s_ID_EX_sltu = '1') then
      s_ex_fwd_data_spec <= s_sltuD;
    elsif (s_ID_EX_upper_imm = '1') then
      s_ex_fwd_data_spec <= s_upper_immD;
    elsif (s_ID_EX_Jump = '1') then
      s_ex_fwd_data_spec <= s_ID_EX_PC;
    else
      s_ex_fwd_data_spec <= s_ALUOut;
    end if;
  end process;

  process (s_ID_EX_rtD, s_ALUSrcD, s_ALUOut)
  begin
    if ((s_ID_EX_rtD(31) = '0') and (s_ALUSrcD(31) = '1')) then
	s_sltuD <= x"00000001";
    elsif ((s_ID_EX_rtD(31) = '1') and (s_ALUSrcD(31) = '0')) then
	s_sltuD <= x"00000000";
    else
	s_sltuD <= s_ALUOut;
    end if;
  end process;

  --EX/MEM Register
  EX_MEM : reg_EX_MEM
   generic map(N => 32)
   port map(i_CLK => iCLK,
        i_RST => iRST,
        i_WE => '1',
	i_upper_imm => s_ID_EX_upper_imm, 
	i_sltu => s_ID_EX_sltu, 
	i_jal => s_ID_EX_Jump, 
	i_MemtoReg => s_ID_EX_MemtoReg, 
	i_RegWrite => s_ID_EX_RegWrite, 
	i_MemWrite => s_ID_EX_MemWrite, 
        i_ALUout => s_ALUOut,
	i_ALUb => s_ALU_mux_BD,
	i_WrAddr => s_EX_MEM_RegDst_muxD,
	i_sltuD => s_sltuD, 
	i_upper_immD => s_upper_immD, 
	i_PC => s_ID_EX_PC,
	i_v0 => s_ID_EX_v0,
	i_Inst => s_ID_EX_Inst,
	o_upper_imm => s_EX_MEM_upper_imm, 
	o_sltu => s_EX_MEM_sltu, 
	o_jal => s_EX_MEM_Jump, 
	o_MemtoReg => s_EX_MEM_MemtoReg, 
	o_RegWrite => s_EX_MEM_RegWrite, 
	o_MemWrite => s_DMemWr,
        o_ALUout => s_DmemAddr,
	o_ALUb => s_DMemData,
	o_WrAddr => s_EX_MEM_WrAddr,
	o_sltuD => s_EX_MEM_sltuD, 
	o_upper_immD => s_EX_MEM_upper_immD, 
	o_PC => s_EX_MEM_PC,
	o_v0 => s_EX_MEM_v0,
	o_Inst => s_EX_MEM_Inst);

  --MEM Stage
  mem_fwd_sel : process (s_EX_MEM_sltu, s_EX_MEM_upper_imm, s_EX_MEM_sltuD, s_EX_MEM_upper_immD, s_DMemAddr, s_EX_MEM_PC, s_EX_MEM_Jump)
  begin
    if (s_EX_MEM_sltu = '1') then
      s_fwd_data_spec <= s_EX_MEM_sltuD;
    elsif (s_EX_MEM_upper_imm = '1') then
      s_fwd_data_spec <= s_EX_MEM_upper_immD;
    elsif (s_EX_MEM_Jump = '1') then
      s_fwd_data_spec <= s_EX_MEM_PC;
    else
      s_fwd_data_spec <= s_DMemAddr;
    end if;
  end process;


  --MEM/WB Register
  MEM_WB : reg_MEM_WB
   port map(i_CLK => iCLK, 
	i_RST => iRST, 
	i_WE => '1',
	i_upper_imm => s_EX_MEM_upper_imm, 
	i_sltu => s_EX_MEM_sltu, 
	i_jal => s_EX_MEM_Jump, 
	i_MemtoReg => s_EX_MEM_MemtoReg, 
	i_RegWrite => s_EX_MEM_RegWrite,
	i_DMEM => s_DMemOut, 
	i_ALU => s_DMemAddr, 
	i_upper_immD => s_EX_MEM_upper_immD, 
	i_sltuD => s_EX_MEM_sltuD, 
	i_PC => s_EX_MEM_PC,
	i_WrAddr => s_EX_MEM_WrAddr,
	i_v0 => s_EX_MEM_v0,
	i_Inst => s_EX_MEM_Inst,
	o_upper_imm => s_MEM_WB_upper_imm, 
	o_sltu => s_MEM_WB_sltu, 
	o_jal => s_MEM_WB_Jump, 
	o_MemtoReg => s_MEM_WB_MemtoReg, 
	o_RegWrite => s_RegWr,
	o_DMEM => s_MEM_WB_DMemOut, 
	o_ALU => s_MEM_WB_ALUOut, 
	o_upper_immD => s_MEM_WB_upper_immD, 
	o_sltuD => s_MEM_WB_sltuD, 
	o_PC => s_MEM_WB_PC,
	o_WrAddr => s_RegDst_muxD,
	o_v0 => open,
	o_Inst => s_Inst);

  MemtoReg_mux : Nmux_dataflow
    generic map(N => 32)
    port map(i_A => s_MEM_WB_ALUOut,
	     i_B => s_MEM_WB_DMemOut,
	     i_S => s_MEM_WB_MemtoReg,
	     o_F => s_MemtoRegD);

  UpperImm_mux : Nmux_dataflow
    generic map(N => 32)
    port map(i_A => s_MemtoRegD,
	     i_B => s_MEM_WB_upper_immD,
	     i_S => s_MEM_WB_upper_imm,
	     o_F => s_upper_imm_muxD);

  sltu_mux : Nmux_dataflow
    generic map(N => 32)
    port map(i_A => s_upper_imm_muxD,
	     i_B => s_MEM_WB_sltuD,
	     i_S => s_MEM_WB_sltu,
	     o_F => s_sltu_muxD);

  jal_write_mux : Nmux_dataflow
    port map(i_A => s_sltu_muxD,
	     i_B => s_MEM_WB_PC,
	     i_S => s_MEM_WB_Jump,
	     o_F => s_RegWrData);


  halt : process (v0, s_Halt)
  begin
    if (v0 = x"0000000A") then
      s_Halt <= '1';
    else 
      s_Halt <= '0';
    end if;
  end process;

end structure;