-------------------------------------------------------------------------
-- Trevor Nemes & Zach Johnson
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- reg_EX_MEM.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the EX/MEM pipeline
-- register
--
-- NOTES:
-- 11/18/2019 by TJN & ZJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity reg_EX_MEM is
   generic(N: integer := 32);
   port(i_CLK       			: in std_logic;  
        i_RST        			: in std_logic;
        i_WE         			: in std_logic;
	i_upper_imm, i_sltu, i_jal, i_MemtoReg, i_RegWrite, i_MemWrite  : in std_logic;    
        i_ALUout			: in std_logic_vector(N-1 downto 0);
	i_ALUb	     			: in std_logic_vector(N-1 downto 0);  
	i_WrAddr			: in std_logic_vector(4 downto 0);
	i_sltuD, i_upper_immD, i_PC, i_v0, i_Inst	: in std_logic_vector(N-1 downto 0);
	o_upper_imm, o_sltu, o_jal, o_MemtoReg, o_RegWrite, o_MemWrite  : out std_logic; 
        o_ALUout        		: out std_logic_vector(N-1 downto 0);
	o_ALUb	     			: out std_logic_vector(N-1 downto 0);
	o_WrAddr			: out std_logic_vector(4 downto 0);
	o_sltuD, o_upper_immD, o_PC, o_v0, o_Inst	: out std_logic_vector(N-1 downto 0));   
end reg_EX_MEM;

architecture structure of reg_EX_MEM is

component nbit_register
generic(N: integer := 32);
   port(i_CLK        : in std_logic;  
        i_RST        : in std_logic;
        i_WE         : in std_logic;    
        i_D          : in std_logic_vector(N-1 downto 0);   
        o_Q          : out std_logic_vector(N-1 downto 0));
end component;

component dflipflop
   port(i_CLK        : in std_logic;   
       i_RST        : in std_logic;    
       i_WE         : in std_logic;     
       i_D          : in std_logic;     
       o_Q          : out std_logic); 
end component;

begin

ALUout : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_ALUout,
	     o_Q => o_ALUout);

Inst_reg : nbit_register
  generic map(N => 32)
  port map(i_CLK => i_CLK,
	   i_RST => i_RST,
	   i_WE => i_WE,
	   i_D => i_Inst,
	   o_Q => o_Inst);

v0 : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_v0,
	     o_Q => o_v0);

ALUb : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_ALUb,
	     o_Q => o_ALUb);

WrAddr : nbit_register
    generic map(N => 5)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_WrAddr,
	     o_Q => o_WrAddr);

sltuD : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_sltuD,
	     o_Q => o_sltuD);

upper_immD : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_upper_immD,
	     o_Q => o_upper_immD);

upper_imm : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_upper_imm,
	    o_Q => o_upper_imm);

sltu : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_sltu,
	    o_Q => o_sltu);

jal : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_jal,
	    o_Q => o_jal);

MemtoReg : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_MemtoReg,
	    o_Q => o_MemtoReg);

RegWrite : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_RegWrite,
	    o_Q => o_RegWrite);

MemWrite : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_MemWrite,
	    o_Q => o_MemWrite);

PC : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_PC,
	     o_Q => o_PC);

end structure;