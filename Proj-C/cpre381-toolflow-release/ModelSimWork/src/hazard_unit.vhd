-------------------------------------------------------------------------
-- Trevor Nemes & Zach Johnson
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- hazard_unit.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a hazard detection unit

-- 12/7/2019 by TN,ZJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity hazard_unit is
  generic(N : integer := 32);
  port(branch, jump, ID_EX_Branch					: in std_logic;
       ID_EX_MemtoReg, jr, ID_EX_Jump, ID_EX_jr, EX_MEM_MemtoReg	: in std_logic;
       IF_ID_rd, IF_ID_rt, EX_MEM_RegDst_muxD, EX_MEM_WrAddr		: in std_logic_vector(4 downto 0);
       ID_EX_Inst, EX_MEM_Inst						: in std_logic_vector(31 downto 0);
       IF_ID_flush, IF_ID_stall, ID_EX_stall, PC_stall, ID_EX_flush	: out std_logic);
end hazard_unit;

architecture dataflow of hazard_unit is

begin

IF_ID_flush <= '1' when ((ID_EX_Branch = '1') or (ID_EX_Jump = '1') or (ID_EX_jr = '1')) else
		'0';

ID_EX_flush <= '1' when ((EX_MEM_MemtoReg = '1') and (ID_EX_MemtoReg = '1') and (ID_EX_Inst = EX_MEM_Inst)) else
		'0';

ID_EX_stall <= '0' when (((jr = '1') or (ID_EX_MemtoReg = '1')) and (((EX_MEM_RegDst_muxD = IF_ID_rt) and (EX_MEM_RegDst_muxD  /= "00000")) or ((EX_MEM_RegDst_muxD = IF_ID_rd) and (EX_MEM_RegDst_muxD  /= "00000")))) else
		'1';

IF_ID_stall <= '0' when (((ID_EX_MemtoReg = '1') and ((EX_MEM_RegDst_muxD = IF_ID_rt) or (EX_MEM_RegDst_muxD = IF_ID_rd)) and (EX_MEM_RegDst_muxD  /= "00000")) or (branch = '1') or (jump = '1') or (jr = '1')) else
		'1';

PC_stall <= '0' when ((((ID_EX_MemtoReg = '1') or (jr = '1')) and ((EX_MEM_RegDst_muxD = IF_ID_rt) or (EX_MEM_RegDst_muxD = IF_ID_rd)) and (EX_MEM_RegDst_muxD /= "00000")) or  (ID_EX_Branch = '1') or (ID_EX_Jump = '1') or (ID_EX_jr = '1')) else
		'1';

end dataflow;