-------------------------------------------------------------------------
-- Trevor Nemes & Zach Johnson
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- reg_ID_EX.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the ID/EX pipeline
-- register
--
-- NOTES:
-- 11/18/2019 by TJN & ZJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity reg_ID_EX is
   generic(N: integer := 32);
   port(i_CLK       			: in std_logic;  
        i_RST        			: in std_logic;
        i_WE         			: in std_logic;
	i_upper_imm, i_RegDst, i_sltu, i_jal, i_MemtoReg, i_RegWrite, i_MemWrite, i_ALUSrc, i_AorL, i_RorL, i_ALUorShifter, i_ShiftVariable, i_Branch, i_jr  : in std_logic;
	i_Op				: in std_logic_vector(2 downto 0);      
	i_WrAddr, i_rd, i_rt, i_shamt	: in std_logic_vector(4 downto 0);
	i_rtD, i_rdD			: in std_logic_vector(N-1 downto 0);
	i_imm_ext, i_PC, i_v0, i_Inst			: in std_logic_vector(N-1 downto 0);
	o_upper_imm, o_RegDst, o_sltu, o_jal, o_MemtoReg, o_RegWrite, o_MemWrite, o_ALUSrc, o_AorL, o_RorL, o_ALUorShifter, o_ShiftVariable, o_Branch, o_jr  : out std_logic; 
	o_Op				: out std_logic_vector(2 downto 0);  
	o_WrAddr, o_rd, o_rt, o_shamt	: out std_logic_vector(4 downto 0);
	o_rtD, o_rdD			: out std_logic_vector(N-1 downto 0);
	o_imm_ext, o_PC, o_v0, o_Inst			: out std_logic_vector(N-1 downto 0));   
end reg_ID_EX;

architecture structure of reg_ID_EX is

component nbit_register
generic(N: integer := 32);
   port(i_CLK        : in std_logic;  
        i_RST        : in std_logic;
        i_WE         : in std_logic;    
        i_D          : in std_logic_vector(N-1 downto 0);   
        o_Q          : out std_logic_vector(N-1 downto 0));
end component;

component dflipflop
   port(i_CLK        : in std_logic;   
       i_RST        : in std_logic;    
       i_WE         : in std_logic;     
       i_D          : in std_logic;     
       o_Q          : out std_logic); 
end component;

begin

WrAddr : nbit_register
    generic map(N => 5)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_WrAddr,
	     o_Q => o_WrAddr);

Inst_reg : nbit_register
  generic map(N => 32)
  port map(i_CLK => i_CLK,
	   i_RST => i_RST,
	   i_WE => i_WE,
	   i_D => i_Inst,
	   o_Q => o_Inst);

v0 : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_v0,
	     o_Q => o_v0);

rd : nbit_register
    generic map(N => 5)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_rd,
	     o_Q => o_rd);

rt : nbit_register
    generic map(N => 5)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_rt,
	     o_Q => o_rt);

shamt : nbit_register
    generic map(N => 5)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_shamt,
	     o_Q => o_shamt);

rtD : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_rtD,
	     o_Q => o_rtD);

rdD : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_rdD,
	     o_Q => o_rdD);

imm_ext : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_imm_ext,
	     o_Q => o_imm_ext);

Op : nbit_register
    generic map(N => 3)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_Op,
	     o_Q => o_Op);

upper_imm : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_upper_imm,
	    o_Q => o_upper_imm);

sltu : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_sltu,
	    o_Q => o_sltu);

jal : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_jal,
	    o_Q => o_jal);

MemtoReg : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_MemtoReg,
	    o_Q => o_MemtoReg);

RegWrite : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_RegWrite,
	    o_Q => o_RegWrite);

MemWrite : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_MemWrite,
	    o_Q => o_MemWrite);

ALUSrc : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_ALUSrc,
	    o_Q => o_ALUSrc);

AorL : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_AorL,
	    o_Q => o_AorL);

RorL : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_RorL,
	    o_Q => o_RorL);

ALUorShifter : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_ALUorShifter,
	    o_Q => o_ALUorShifter);

ShiftVariable : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_ShiftVariable,
	    o_Q => o_ShiftVariable);

RegDst : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_RegDst,
	    o_Q => o_RegDst);

Branch : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_Branch,
	    o_Q => o_Branch);

jr : dflipflop
   port MAP(i_CLK => i_CLK,
	    i_RST => i_RST,
 	    i_WE => i_WE,
	    i_D => i_jr,
	    o_Q => o_jr);
PC : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_PC,
	     o_Q => o_PC);

end structure;