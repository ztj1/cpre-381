-------------------------------------------------------------------------
-- Zachary Johnson and Trevor Nemes
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- reg_MEM_WB.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: MEM/WB pipeline register
-- 
--
--
-- NOTES:
-- 11/19/2019 by ZTJ&TJN::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity reg_MEM_WB is
   port(i_CLK, i_RST, i_WE					: in std_logic;
	i_upper_imm, i_sltu, i_jal, i_MemtoReg, i_RegWrite	: in std_logic;
	i_DMEM, i_ALU, i_upper_immD, i_sltuD, i_PC, i_v0, i_Inst			: in std_logic_vector(31 downto 0);
	i_WrAddr						: in std_logic_vector(4 downto 0);
	o_upper_imm, o_sltu, o_jal, o_MemtoReg, o_RegWrite	: out std_logic;
	o_DMEM, o_ALU, o_upper_immD, o_sltuD, o_PC, o_v0, o_Inst			: out std_logic_vector(31 downto 0);
	o_WrAddr						: out std_logic_vector(4 downto 0));
end reg_MEM_WB;

architecture structure of reg_MEM_WB is

component nbit_register is
   generic(N: integer := 32);
   port(i_CLK        : in std_logic;  
        i_RST        : in std_logic;
        i_WE         : in std_logic;    
        i_D          : in std_logic_vector(N-1 downto 0);   
        o_Q          : out std_logic_vector(N-1 downto 0));
end component;

component dflipflop is
   port(i_CLK        : in std_logic;   
        i_RST        : in std_logic;    
        i_WE         : in std_logic;     
        i_D          : in std_logic;     
        o_Q          : out std_logic); 
 end component;

begin

dmem_reg : nbit_register
  generic map(N => 32)
  port map(i_CLK => i_CLK,
	   i_RST => i_RST,
	   i_WE => i_WE,
	   i_D => i_DMEM,
	   o_Q => o_DMEM);

Inst_reg : nbit_register
  generic map(N => 32)
  port map(i_CLK => i_CLK,
	   i_RST => i_RST,
	   i_WE => i_WE,
	   i_D => i_Inst,
	   o_Q => o_Inst);

v0 : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_v0,
	     o_Q => o_v0);

ALU_reg : nbit_register
  generic map(N => 32)
  port map(i_CLK => i_CLK,
	   i_RST => i_RST,
	   i_WE => i_WE,
	   i_D => i_ALU,
	   o_Q => o_ALU);

wraddr_reg : nbit_register
  generic map(N => 5)
  port map(i_CLK => i_CLK,
	   i_RST => i_RST,
	   i_WE => i_WE,
	   i_D => i_WrAddr,
	   o_Q => o_WrAddr);

sltuD_reg : nbit_register
  generic map(N => 32)
  port map(i_CLK => i_CLK,
	   i_RST => i_RST,
	   i_WE => i_WE,
	   i_D => i_sltuD,
	   o_Q => o_sltuD);

upper_immD_reg : nbit_register
  generic map(N => 32)
  port map(i_CLK => i_CLK,
	   i_RST => i_RST,
	   i_WE => i_WE,
	   i_D => i_upper_immD,
	   o_Q => o_upper_immD);

--JumpAddr_reg : nbit_register
--  generic map(N => 5);
--  port map(i_CLK => i_CLK,
--	   i_RST => i_RST,
--	   i_WE => i_WE,
--	   i_D => i_JumpAddr,
--	   o_Q => o_JumpAddr);

upper_imm_reg : dflipflop
  port map(i_CLK => i_CLK,
	   i_RST => i_RST,
	   i_WE => i_WE,
	   i_D => i_upper_imm,
	   o_Q => o_upper_imm);

sltu_reg : dflipflop
  port map(i_CLK => i_CLK,
	   i_RST => i_RST,
	   i_WE => i_WE,
	   i_D => i_sltu,
	   o_Q => o_sltu);

jal_reg : dflipflop
  port map(i_CLK => i_CLK,
	   i_RST => i_RST,
	   i_WE => i_WE,
	   i_D => i_jal,
	   o_Q => o_jal);

MemtoReg_reg : dflipflop
  port map(i_CLK => i_CLK,
	   i_RST => i_RST,
	   i_WE => i_WE,
	   i_D => i_MemtoReg,
	   o_Q => o_MemtoReg);

RegWrite_reg : dflipflop
  port map(i_CLK => i_CLK,
	   i_RST => i_RST,
	   i_WE => i_WE,
	   i_D => i_RegWrite,
	   o_Q => o_RegWrite);

PC : nbit_register
    generic map(N => 32)
    port map(i_CLK => i_CLK,
	     i_RST => i_RST,
	     i_WE => i_WE,
	     i_D => i_PC,
	     o_Q => o_PC);

end structure;