-------------------------------------------------------------------------
-- Trevor Nemes & Zach Johnson
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- fwd_unit.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a hazard detection unit

-- 12/7/2019 by TN,ZJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity fwd_unit is
  port(EX_MEM_RegWrite, MEM_WB_RegWrite, jr, beq, bne, sltu, upper_imm				: in std_logic;
       ID_EX_rd, ID_EX_rt, EX_MEM_WrAddr, MEM_WB_WrAddr, IF_ID_rt, IF_ID_rd, EX_RegDst		: in std_logic_vector(4 downto 0);
       fwd_mux_C  						: out std_logic;
       fwd_mux_A, fwd_mux_B, fwd_mux_D, fwd_mux_E				: out std_logic_vector(1 downto 0));
end fwd_unit;

architecture behavior of fwd_unit is

begin

process(EX_MEM_WrAddr, ID_EX_rt, MEM_WB_RegWrite, MEM_WB_WrAddr, ID_EX_rd, IF_ID_rd, IF_ID_rt, EX_RegDst, beq, bne, jr, EX_MEM_RegWrite, sltu, upper_imm)
begin

if ((EX_MEM_RegWrite = '1') and (EX_MEM_WrAddr /= "00000") and (EX_MEM_WrAddr = ID_EX_rt)) then
	fwd_mux_A <= "10";
elsif ((MEM_WB_RegWrite = '1') and (MEM_WB_WrAddr /= "00000") and (MEM_WB_WrAddr = ID_EX_rt)) then
	fwd_mux_A <= "01";
else
	fwd_mux_A <= "00";
end if;


if ((EX_MEM_RegWrite = '1') and (EX_MEM_WrAddr /= "00000") and (EX_MEM_WrAddr = ID_EX_rd)) then
	fwd_mux_B <= "10";
elsif ((MEM_WB_RegWrite = '1') and (MEM_WB_WrAddr /= "00000") and (MEM_WB_WrAddr = ID_EX_rd)) then
	fwd_mux_B <= "01";
else
	fwd_mux_B <= "00";
end if;


if ((jr = '1') and (EX_MEM_WrAddr = "11111")) then
	fwd_mux_C <= '1';
else
	fwd_mux_C <= '0';
end if;


if (((beq = '1') or (bne = '1') or (jr = '1') or (sltu = '1') or (upper_imm = '1')) and (EX_RegDst = IF_ID_rt)) then
	fwd_mux_D <= "10";
elsif (((beq = '1') or (bne = '1') or (jr = '1') or (sltu = '1') or (upper_imm = '1')) and (EX_MEM_WrAddr = IF_ID_rt)) then
	fwd_mux_D <= "11";
elsif ((MEM_WB_RegWrite = '1') and (MEM_WB_WrAddr = IF_ID_rt)) then
	fwd_mux_D <= "01";
else
	fwd_mux_D <= "00";
end if;



if (((beq = '1') or (bne = '1') or (sltu = '1') or (upper_imm = '1')) and (EX_RegDst = IF_ID_rd)) then
	fwd_mux_E <= "10";
elsif (((beq = '1') or (bne = '1') or (sltu = '1') or (upper_imm = '1')) and (EX_MEM_WrAddr = IF_ID_rd)) then
	fwd_mux_E <= "11";
elsif ((MEM_WB_RegWrite = '1') and (MEM_WB_WrAddr = IF_ID_rd)) then
	fwd_mux_E <= "01";
else
	fwd_mux_E <= "00";
end if;

end process;

end behavior;