-------------------------------------------------------------------------
-- Trevor Nemes & Zach Johnson
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- Nmux_4to1.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a N-bit 4-1 mux 
-- dataflow structure
--
--
-- NOTES:
-- 9/5/2019 by TJN::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity Nmux_4to1 is
	generic(N : integer := 32);
	port(i_A : in std_logic_vector(N-1 downto 0);	--00
       	     i_B : in std_logic_vector(N-1 downto 0);	--01
	     i_C : in std_logic_vector(N-1 downto 0);   --10
	     i_D : in std_logic_vector(N-1 downto 0);   --11
             i_S : in std_logic_vector(1 downto 0);
             o_F : out std_logic_vector(N-1 downto 0));
end Nmux_4to1;

architecture structure of Nmux_4to1 is

signal s_mux1, s_mux2 : std_logic_vector(31 downto 0);

begin

process(i_A, i_B, i_C, i_D,  i_S)
begin

   if i_S = "00" then
	o_F <= i_A;
   elsif i_S = "01" then
	o_F <= i_B;
   elsif i_S = "10" then
	o_F <= i_C;
   else
	o_F <= i_D;
   end if; 

end process; 

end structure;