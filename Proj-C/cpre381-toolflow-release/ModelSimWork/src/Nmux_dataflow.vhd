-------------------------------------------------------------------------
-- Trevor Nemes
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- 2-1Nmux_dataflow.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a N-bit 2-1 mux 
-- dataflow structure
--
--
-- NOTES:
-- 9/5/2019 by TJN::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity Nmux_dataflow is
	generic(N : integer := 32);
	port(i_A : in std_logic_vector(N-1 downto 0);	--0
       	     i_B : in std_logic_vector(N-1 downto 0);	--1
             i_S : in std_logic;
             o_F : out std_logic_vector(N-1 downto 0));

end Nmux_dataflow;

architecture dataflow of Nmux_dataflow is

	signal s_Ss : std_logic_vector(N-1 downto 0);
	signal s_S : std_logic_vector(N-1 downto 0);
	signal s_A : std_logic_vector(N-1 downto 0);
	signal s_B : std_logic_vector(N-1 downto 0);

begin

   G1: for i in 0 to N-1 generate
	s_Ss(i) <= i_S;
   end generate;

	s_S <= NOT s_Ss;
	s_A <= i_A AND s_S;
	s_B <= i_B AND s_Ss;
	o_F <= s_A OR s_B;

end dataflow;