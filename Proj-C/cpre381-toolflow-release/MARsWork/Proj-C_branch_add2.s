#
# Second part of the Lab 3 test program
#
# MARsWork/Proj-C_branch_add2.s


not_branch_here:

addi $1, $0, 1
addi $2, $0, 2
addi $3, $0, -3
addi $4, $0, -4
addi $5, $0, 5
addi $6, $0, 6

bne $1, $1, not_branch_here
bne $1, $2, bne_here

addi $15, $1, 2

bne_here:

bne $3, $3, not_branch_here
bne $3, $4, bne2_here

addi $16, $1, 1

bne2_here:

beq $1, $2, not_branch_here
beq $1, $1, beq_here

addi $17, $1, 1

beq_here:

beq $3, $4, not_branch_here
beq $3, $3, beq2_here

addi $18, $1, 1

beq2_here:

addi $15, $1, 5
add $16, $5, $6


addi  $2,  $0,  10              # Place 10 in $v0 to signal a halt
syscall                         # Actually cause the halt