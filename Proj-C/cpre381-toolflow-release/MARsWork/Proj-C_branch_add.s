#
# Second part of the Lab 3 test program
#
# MARsWork/Proj-C_branch_add.s

# data section

addi $1, $0, 1
addi $2, $1, 11

beq $1, $1, bne_here

addi $1, $2, 7

bne_here:

addi $1, $1, 10

addi  $2,  $0,  10              # Place 10 in $v0 to signal a halt
syscall                         # Actually cause the halt