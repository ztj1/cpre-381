#
# Bubble sort part of the Lab 3 test program
#
# MARsWork/Proj-C_test2.s

.data
arr:.word   3, 2, 10, 17, 1, 4, 22, 4, 5, 7
size:.word  40

.text
.globl main
main:

#la $t0, arr
lui $at, 4097

lw $t3, size

addi $t1, $0, 0			# i
addi $t2, $0, 0			# count

ori $t0, $at, 0
NOP

j count_loop
NOP
NOP
swap:
	sw $s0, 4($t4)		# swap
	sw $s1, 0($t4)
	j continue
	NOP
	NOP

count_loop:
	sort_loop:
		add $t4, $t0, $t1	# add offset i to arr
		NOP
		NOP
		NOP
		lw $s0, 0($t4)		# load arr[i]
		lw $s1, 4($t4)		# load arr[i+1]
		NOP
		NOP
		NOP
		slt $t5, $s1, $s0	# compare
		NOP
		NOP
		NOP
		bne $t5, $0, swap
		NOP
		NOP
		continue:
		addi $t1, $t1, 4	# increment i
		NOP
		NOP
		NOP
		addi $t6, $t1, 4	# calculate i + 1
		sub $t7, $t3, $t2	# size - count
		NOP
		NOP
		NOP
		slt $s2, $t6, $t7
		NOP
		NOP
		NOP
		bne $s2, $0, sort_loop	# loop if i + 1 equals size - count
		NOP
		NOP
	addi $t2, $t2, 4
	addi $t1, $0, 0
	NOP
	NOP
	slt $s3, $t2, $t3
	NOP
	NOP
	NOP
	bne $s3, $0, count_loop
	NOP
	NOP
	
	
addi  $2,  $0,  10              # Place 10 in $v0 to signal a halt
syscall                         # Actually cause the halt

#int N = 10;
#int i = 0;
#int count = 0;

#while (count < N) {
#	while (i + 1) < (N - count) {
#		if (arr[i] > arr[i+1] {
#			swap(arr[i], arr[i+1]):
#		}
#		i++;
#	}
#	count++;
#}