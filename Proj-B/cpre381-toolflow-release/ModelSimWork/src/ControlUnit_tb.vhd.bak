-------------------------------------------------------------------------
-- Trevor Nemes & Zach Johnson
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- ControlUnit_tb.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the Control 
-- components testbench
--
-- NOTES:
-- 10/19/2019 by TJN & ZJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity ControlUnit_tb is
end ControlUnit_tb;

architecture behavior of ControlUnit_tb is

   component ControlUnit 
	port(op_Code        : in std_logic_vector(5 downto 0);
	     Funct	       : in std_logic_vector(5 downto 0);
	     o_Op          : out std_logic_vector(2 downto 0);
	     RegDst, o_beq, o_bne, Jump, jr, MemtoReg, MemWrite, ALUSrc, RegWrite : out std_logic;
	     o_Signed, o_AorL, o_RorL, o_ALUorShifter, o_ShiftVariable, o_upper_imm, o_sltu : out std_logic);
   end component;

   signal s_opCode, s_Funct : std_logic_vector(5 downto 0);
   signal s_Op : std_logic_vector(2 downto 0);
   signal s_RegDst, s_Jump, s_Branch, s_MemtoReg, s_MemWrite, s_ALUSrc, s_RegWrite, s_Signed, s_AorL, s_RorL, s_ALUorShifter, s_ShiftVariable, s_upper_imm, s_sltu : std_logic;

begin

   g_ControlUnit : ControlUnit
	port MAP(s_opCode, s_Funct, s_Op, s_RegDst, s_Jump, s_Branch, s_MemtoReg, s_MemWrite, s_ALUSrc, s_RegWrite, s_Signed, s_AorL, s_RorL, s_ALUorShifter, s_ShiftVariable, s_upper_imm, s_sltu);

   p_test : process
   begin

	s_opCode <= "001000";	--addi
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "000000";	--add
	s_Funct <= "100000";
	wait for 50 ns;

	s_opCode <= "001001";	--addiu
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "000000";	--addu
	s_Funct <= "100001";
	wait for 50 ns;

	s_opCode <= "000000";	--and
	s_Funct <= "100100";
	wait for 50 ns;

	s_opCode <= "001100";	--andi
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "001111";	--lui
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "100011";	--lw
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "000000";	--nor
	s_Funct <= "100111";
	wait for 50 ns;

	s_opCode <= "000000";	--xor
	s_Funct <= "100110";
	wait for 50 ns;

	s_opCode <= "001110";	--xori
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "000000";	--or
	s_Funct <= "100101";
	wait for 50 ns;

	s_opCode <= "001101";	--ori
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "000000";	--slt
	s_Funct <= "101010";
	wait for 50 ns;

	s_opCode <= "001010";	--slti
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "001011";	--sltiu
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "000000";	--sltu
	s_Funct <= "101011";
	wait for 50 ns;

	s_opCode <= "000000";	--sll
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "000000";	--srl
	s_Funct <= "000010";
	wait for 50 ns;

	s_opCode <= "000000";	--sra
	s_Funct <= "000011";
	wait for 50 ns;

	s_opCode <= "000000";	--sllv
	s_Funct <= "000100";
	wait for 50 ns;

	s_opCode <= "000000";	--srlv
	s_Funct <= "000110";
	wait for 50 ns;

	s_opCode <= "000000";	--srav
	s_Funct <= "000111";
	wait for 50 ns;

	s_opCode <= "101011";	--sw
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "000000";	--sub
	s_Funct <= "100010";
	wait for 50 ns;

	s_opCode <= "000000";	--subu
	s_Funct <= "100011";
	wait for 50 ns;

	s_opCode <= "000100";	--beq
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "000101";	--bne
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "000010";	--j
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "000011";	--jal
	s_Funct <= "000000";
	wait for 50 ns;

	s_opCode <= "000000";	--jr
	s_Funct <= "001000";
	wait for 50 ns;

	wait;

   end process;

end behavior;