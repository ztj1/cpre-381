-------------------------------------------------------------------------
-- Trevor Nemes
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- RegFile.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a Register file
--
--
--
-- NOTES:
-- 9/11/2019 by TJN::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.reg_array_type.all;

entity RegFile is
   generic(N : integer := 32);
   port(i_CLK : in std_logic;
	i_RST : in std_logic;
	rt : in std_logic_vector(4 downto 0);
	rd : in std_logic_vector(4 downto 0);
	rw : in std_logic_vector(4 downto 0);
	WE : in std_logic;
	Data : in std_logic_vector(N-1 downto 0);
	o_2  : out std_logic_vector(N-1 downto 0);
	o_rt : out std_logic_vector(N-1 downto 0);
	o_rd : out std_logic_vector(N-1 downto 0));
end RegFile;

architecture structural of RegFile is

   signal s_rw : std_logic_vector(31 downto 0);
   signal WE_RW : std_logic_vector(31 downto 0);
   signal o_Q : regArray;

   component nbit_register
   generic(N: integer := 32);
   port(i_CLK        : in std_logic;  
        i_RST        : in std_logic;
        i_WE         : in std_logic;    
        i_D          : in std_logic_vector(N-1 downto 0);   
        o_Q          : out std_logic_vector(N-1 downto 0));  
   end component;

   component Decoder
	port(i_A : in std_logic_vector(4 downto 0);
	     o_D : out std_logic_vector(31 downto 0));
   end component;

   component mux32
	port(i_A : in regArray;
	     i_S : in std_logic_vector(4 downto 0);
	     o_D : out std_logic_vector(31 downto 0));
   end component;

begin

g_Decoder : Decoder
   port MAP(rw, s_rw);
re_mux_t : mux32
   port MAP(o_Q, rt, o_rt);
re_mux_d : mux32
   port MAP(o_Q, rd, o_rd);

reg_0 : nbit_register
port MAP(i_CLK => i_CLK,
	i_RST => '1',
	i_WE => '0',
	i_D => x"00000000",
	o_Q => o_Q(0));

G1: for i in 1 to 31 generate
   WE_RW(i) <= WE and s_rw(i);
   reg_i : nbit_register
   port MAP(i_CLK => i_CLK,
	i_RST => i_RST,
	i_WE => WE_RW(i),
	i_D => Data,
	o_Q => o_Q(i));
   end generate;

o_2 <= o_Q(2);

end structural;
  