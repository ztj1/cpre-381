-------------------------------------------------------------------------
-- Trevor Nemes & Zach Johnson
-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- ControlUnit.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the Control 
-- component
--
-- NOTES:
-- 10/19/2019 by TJN & ZJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity ControlUnit is
   port(op_Code        : in std_logic_vector(5 downto 0);
	Funct	       : in std_logic_vector(5 downto 0);
	o_Op          : out std_logic_vector(2 downto 0);
	RegDst, o_beq, o_bne, Jump, jr, MemtoReg, MemWrite, ALUSrc, RegWrite : out std_logic;
	o_Signed, o_AorL, o_RorL, o_ALUorShifter, o_ShiftVariable, o_upper_imm, o_sltu : out std_logic);
end ControlUnit;

architecture behavior of ControlUnit is

begin

  process(op_Code, Funct)

  begin

	if op_Code =  "000000" then			-- R-format

		   if Funct = "100000" then	-- add
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '1';
			o_Op <= "000";
			o_AorL <= '0';
			o_RorL <= '0';
			o_ALUorShifter <= '0';
			o_ShiftVariable <= '0';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   elsif Funct = "100001" then	-- addu
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '1';
			o_Op <= "000";
			o_AorL <= '0';
			o_RorL <= '0';
			o_ALUorShifter <= '0';
			o_ShiftVariable <= '0';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   elsif Funct = "100100" then	-- and
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '1';
			o_Op <= "010";
			o_AorL <= '0';
			o_RorL <= '0';
			o_ALUorShifter <= '0';
			o_ShiftVariable <= '0';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   elsif Funct = "100111" then	-- nor
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '1';
			o_Op <= "111";
			o_AorL <= '0';
			o_RorL <= '0';
			o_ALUorShifter <= '0';
			o_ShiftVariable <= '0';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   elsif Funct = "100110" then	-- xor
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '1';
			o_Op <= "101";
			o_AorL <= '0';
			o_RorL <= '0';
			o_ALUorShifter <= '0';
			o_ShiftVariable <= '0';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   elsif Funct = "100101" then	-- or
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '1';
			o_Op <= "100";
			o_AorL <= '0';
			o_RorL <= '0';
			o_ALUorShifter <= '0';
			o_ShiftVariable <= '0';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   elsif Funct = "101010" then	-- slt
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '1';
			o_Op <= "011";
			o_AorL <= '0';
			o_RorL <= '0';
			o_ALUorShifter <= '0';
			o_ShiftVariable <= '0';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   elsif Funct = "101011" then	-- sltu
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '1';
			o_Op <= "011";
			o_AorL <= '0';
			o_RorL <= '0';
			o_ALUorShifter <= '0';
			o_ShiftVariable <= '0';
			o_upper_imm <= '0';
			o_sltu <= '1';

		   elsif Funct = "000000" then	-- sll
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '0';
			o_Op <= "000";
			o_AorL <= '1';
			o_RorL <= '1';
			o_ALUorShifter <= '1';
			o_ShiftVariable <= '0';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   elsif Funct = "000010" then	-- srl
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '0';
			o_Op <= "000";
			o_AorL <= '1';
			o_RorL <= '0';
			o_ALUorShifter <= '1';
			o_ShiftVariable <= '0';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   elsif Funct = "000011" then	-- sra
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '0';
			o_Op <= "000";
			o_AorL <= '0';
			o_RorL <= '0';
			o_ALUorShifter <= '1';
			o_ShiftVariable <= '0';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   elsif Funct = "000100" then	-- sllv
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '0';
			o_Op <= "000";
			o_AorL <= '1';
			o_RorL <= '1';
			o_ALUorShifter <= '1';
			o_ShiftVariable <= '1';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   elsif Funct = "000110" then	-- srlv
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '0';
			o_Op <= "000";
			o_AorL <= '1';
			o_RorL <= '0';
			o_ALUorShifter <= '1';
			o_ShiftVariable <= '1';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   elsif Funct = "000111" then	-- srav
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '0';
			o_Op <= "000";
			o_AorL <= '0';
			o_RorL <= '0';
			o_ALUorShifter <= '1';
			o_ShiftVariable <= '1';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   elsif Funct = "100010" then	-- sub
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '1';
			o_Op <= "001";
			o_AorL <= '0';
			o_RorL <= '0';
			o_ALUorShifter <= '0';
			o_ShiftVariable <= '0';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   elsif Funct = "100011" then	-- subu
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '1';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '0';
			o_Signed <= '1';
			o_Op <= "001";
			o_AorL <= '0';
			o_RorL <= '0';
			o_ALUorShifter <= '0';
			o_ShiftVariable <= '0';
			o_upper_imm <= '0';
			o_sltu <= '0';

		   else				-- jr
			RegDst <= '1';
			ALUSrc <= '0';
			MemtoReg <= '0';
			RegWrite <= '0';
			MemWrite <= '0';
			o_beq <= '0';
			o_bne <= '0';
			Jump <= '0';
			jr <= '1';
			o_Signed <= '1';
			o_Op <= "000";
			o_AorL <= '0';
			o_RorL <= '0';
			o_ALUorShifter <= '0';
			o_ShiftVariable <= '0';
			o_upper_imm <= '0';
			o_sltu <= '0';

		end if;
			

	elsif op_Code = "001000" then		-- addi
		RegDst <= '0';
		ALUSrc <= '1';
		MemtoReg <= '0';
		RegWrite <= '1';
		MemWrite <= '0';
		o_beq <= '0';
		o_bne <= '0';
		Jump <= '0';
		jr <= '0';
		o_Signed <= '1';
		o_Op <= "000";
		o_AorL <= '0';
		o_RorL <= '0';
		o_ALUorShifter <= '0';
		o_ShiftVariable <= '0';
		o_upper_imm <= '0';
		o_sltu <= '0';

	elsif op_Code = "001001" then		-- addiu
		RegDst <= '0';
		ALUSrc <= '1';
		MemtoReg <= '0';
		RegWrite <= '1';
		MemWrite <= '0';
		o_beq <= '0';
		o_bne <= '0';
		Jump <= '0';
		jr <= '0';
		o_Signed <= '1';
		o_Op <= "000";
		o_AorL <= '0';
		o_RorL <= '0';
		o_ALUorShifter <= '0';
		o_ShiftVariable <= '0';
		o_upper_imm <= '0';
		o_sltu <= '0';

	elsif op_Code = "001100" then		-- andi
		RegDst <= '0';
		ALUSrc <= '1';
		MemtoReg <= '0';
		RegWrite <= '1';
		MemWrite <= '0';
		o_beq <= '0';
		o_bne <= '0';
		Jump <= '0';
		jr <= '0';
		o_Signed <= '1';
		o_Op <= "010";
		o_AorL <= '0';
		o_RorL <= '0';
		o_ALUorShifter <= '0';
		o_ShiftVariable <= '0';
		o_upper_imm <= '0';
		o_sltu <= '0';

	elsif op_Code = "001111" then		-- lui
		RegDst <= '0';
		ALUSrc <= '1';
		MemtoReg <= '0';
		RegWrite <= '1';
		MemWrite <= '0';
		o_beq <= '0';
		o_bne <= '0';
		Jump <= '0';
		jr <= '0';
		o_Signed <= '0';
		o_Op <= "000";
		o_AorL <= '1';
		o_RorL <= '1';
		o_ALUorShifter <= '1';
		o_ShiftVariable <= '0';
		o_upper_imm <= '1';
		o_sltu <= '0';

	elsif op_Code = "100011" then		-- lw
		RegDst <= '0';
		ALUSrc <= '1';
		MemtoReg <= '1';
		RegWrite <= '1';
		MemWrite <= '0';
		o_beq <= '0';
		o_bne <= '0';
		Jump <= '0';
		jr <= '0';
		o_Signed <= '1';
		o_Op <= "000";
		o_AorL <= '0';
		o_RorL <= '0';
		o_ALUorShifter <= '0';
		o_ShiftVariable <= '0';
		o_upper_imm <= '0';
		o_sltu <= '0';

	elsif op_Code = "001110" then		-- xori
		RegDst <= '0';
		ALUSrc <= '1';
		MemtoReg <= '0';
		RegWrite <= '1';
		MemWrite <= '0';
		o_beq <= '0';
		o_bne <= '0';
		Jump <= '0';
		jr <= '0';
		o_Signed <= '0';
		o_Op <= "101";
		o_AorL <= '0';
		o_RorL <= '0';
		o_ALUorShifter <= '0';
		o_ShiftVariable <= '0';
		o_upper_imm <= '0';
		o_sltu <= '0';

	elsif op_Code = "001101" then		-- ori
		RegDst <= '0';
		ALUSrc <= '1';
		MemtoReg <= '0';
		RegWrite <= '1';
		MemWrite <= '0';
		o_beq <= '0';
		o_bne <= '0';
		Jump <= '0';
		jr <= '0';
		o_Signed <= '0';
		o_Op <= "100";
		o_AorL <= '0';
		o_RorL <= '0';
		o_ALUorShifter <= '0';
		o_ShiftVariable <= '0';
		o_upper_imm <= '0';
		o_sltu <= '0';

	elsif op_Code = "001010" then		-- slti
		RegDst <= '0';
		ALUSrc <= '1';
		MemtoReg <= '0';
		RegWrite <= '1';
		MemWrite <= '0';
		o_beq <= '0';
		o_bne <= '0';
		Jump <= '0';
		jr <= '0';
		o_Signed <= '1';
		o_Op <= "011";
		o_AorL <= '0';
		o_RorL <= '0';
		o_ALUorShifter <= '0';
		o_ShiftVariable <= '0';
		o_upper_imm <= '0';
		o_sltu <= '0';

	elsif op_Code = "001011" then		-- sltiu
		RegDst <= '0';
		ALUSrc <= '1';
		MemtoReg <= '0';
		RegWrite <= '1';
		MemWrite <= '0';
		o_beq <= '0';
		o_bne <= '0';
		Jump <= '0';
		jr <= '0';
		o_Signed <= '1';
		o_Op <= "011";
		o_AorL <= '0';
		o_RorL <= '0';
		o_ALUorShifter <= '0';
		o_ShiftVariable <= '0';
		o_upper_imm <= '0';
		o_sltu <= '1';

	elsif op_Code = "101011" then		-- sw
		RegDst <= '0';
		ALUSrc <= '1';
		MemtoReg <= '0';
		RegWrite <= '0';
		MemWrite <= '1';
		o_beq <= '0';
		o_bne <= '0';
		Jump <= '0';
		jr <= '0';
		o_Signed <= '1';
		o_Op <= "000";
		o_AorL <= '0';
		o_RorL <= '0';
		o_ALUorShifter <= '0';
		o_ShiftVariable <= '0';
		o_upper_imm <= '0';
		o_sltu <= '0';

	elsif op_Code = "000100" then		-- beq
		RegDst <= '0';
		ALUSrc <= '0';
		MemtoReg <= '0';
		RegWrite <= '0';
		MemWrite <= '0';
		o_beq <= '1';
		o_bne <= '0';
		Jump <= '0';
		jr <= '0';
		o_Signed <= '1';
		o_Op <= "001";
		o_AorL <= '0';
		o_RorL <= '0';
		o_ALUorShifter <= '0';
		o_ShiftVariable <= '0';
		o_upper_imm <= '0';
		o_sltu <= '0';

	elsif op_Code = "000101" then		-- bne
		RegDst <= '0';
		ALUSrc <= '0';
		MemtoReg <= '0';
		RegWrite <= '0';
		MemWrite <= '0';
		o_beq <= '0';
		o_bne <= '1';
		Jump <= '0';
		jr <= '0';
		o_Signed <= '1';
		o_Op <= "001";
		o_AorL <= '0';
		o_RorL <= '0';
		o_ALUorShifter <= '0';
		o_ShiftVariable <= '0';
		o_upper_imm <= '0';
		o_sltu <= '0';

	elsif op_Code = "000010" then		-- j
		RegDst <= '0';
		ALUSrc <= '0';
		MemtoReg <= '0';
		RegWrite <= '0';
		MemWrite <= '0';
		o_beq <= '0';
		o_bne <= '0';
		Jump <= '1';
		jr <= '0';
		o_Signed <= '1';
		o_Op <= "000";
		o_AorL <= '0';
		o_RorL <= '0';
		o_ALUorShifter <= '0';
		o_ShiftVariable <= '0';
		o_upper_imm <= '0';
		o_sltu <= '0';

	elsif op_Code = "000011" then		-- jal
		RegDst <= '0';
		ALUSrc <= '0';
		MemtoReg <= '0';
		RegWrite <= '1';
		MemWrite <= '0';
		o_beq <= '0';
		o_bne <= '0';
		Jump <= '1';
		jr <= '0';
		o_Signed <= '1';
		o_Op <= "000";
		o_AorL <= '0';
		o_RorL <= '0';
		o_ALUorShifter <= '0';
		o_ShiftVariable <= '0';
		o_upper_imm <= '0';
		o_sltu <= '0';

	else
		RegDst <= '0';
		ALUSrc <= '0';
		MemtoReg <= '0';
		RegWrite <= '0';
		MemWrite <= '0';
		o_beq <= '0';
		o_bne <= '0';
		Jump <= '0';
		jr <= '0';
		o_Signed <= '0';
		o_Op <= "000";
		o_AorL <= '0';
		o_RorL <= '0';
		o_ALUorShifter <= '0';
		o_ShiftVariable <= '0';
		o_upper_imm <= '0';
		o_sltu <= '0';
		   
   end if;

  end process;

end behavior;