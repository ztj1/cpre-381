-------------------------------------------------------------------------
-- Henry Duwe
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- MIPS_Processor.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a skeleton of a MIPS_Processor  
-- implementation.

-- 01/29/2019 by H3::Design created.
-------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;

entity MIPS_Processor is
  generic(N : integer := 32);
  port(iCLK            : in std_logic;
       iRST            : in std_logic;
       iInstLd         : in std_logic;
       iInstAddr       : in std_logic_vector(N-1 downto 0);
       iInstExt        : in std_logic_vector(N-1 downto 0);
       oALUOut         : out std_logic_vector(N-1 downto 0)); -- TODO: Hook this up to the output of the ALU. It is important for synthesis that you have this output that can effectively be impacted by all other components so they are not optimized away.

end  MIPS_Processor;


architecture structure of MIPS_Processor is

  -- Required data memory signals
  signal s_DMemWr       : std_logic; -- TODO: use this signal as the final active high data memory write enable signal
  signal s_DMemAddr     : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory address input
  signal s_DMemData     : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory data input
  signal s_DMemOut      : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the data memory output
 
  -- Required register file signals 
  signal s_RegWr        : std_logic; -- TODO: use this signal as the final active high write enable input to the register file
  signal s_RegWrAddr    : std_logic_vector(4 downto 0); -- TODO: use this signal as the final destination register address input
  signal s_RegWrData    : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory data input

  -- Required instruction memory signals
  signal s_IMemAddr     : std_logic_vector(N-1 downto 0); -- Do not assign this signal, assign to s_NextInstAddr instead
  signal s_NextInstAddr : std_logic_vector(N-1 downto 0); -- TODO: use this signal as your intended final instruction memory address input.
  signal s_Inst         : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the instruction signal 

  -- Required halt signal -- for simulation
  signal v0             : std_logic_vector(N-1 downto 0); -- TODO: should be assigned to the output of register 2, used to implement the halt SYSCALL
  signal s_Halt         : std_logic;  -- TODO: this signal indicates to the simulation that intended program execution has completed. This case happens when the syscall instruction is observed and the V0 register is at 0x0000000A. This signal is active high and should only be asserted after the last register and memory writes before the syscall are guaranteed to be completed.

  component mem is
    generic(ADDR_WIDTH : integer;
            DATA_WIDTH : integer);
    port(
          clk          : in std_logic;
          addr         : in std_logic_vector((ADDR_WIDTH-1) downto 0);
          data         : in std_logic_vector((DATA_WIDTH-1) downto 0);
          we           : in std_logic := '1';
          q            : out std_logic_vector((DATA_WIDTH -1) downto 0));
    end component;
  
  -- TODO: You may add any additional signals or components your implementation 
  --       requires below this comment

  component RegFile is
    generic(N : integer := 32);
    port(i_CLK : in std_logic;
	 i_RST : in std_logic;
	 rt : in std_logic_vector(4 downto 0);
	 rd : in std_logic_vector(4 downto 0);
	 rw : in std_logic_vector(4 downto 0);
	 WE : in std_logic;
	 Data : in std_logic_vector(N-1 downto 0);
	 o_2  : out std_logic_vector(N-1 downto 0);
	 o_rt : out std_logic_vector(N-1 downto 0);
	 o_rd : out std_logic_vector(N-1 downto 0));
  end component;
  
  component Nmux_dataflow is
	generic(N : integer := 32);
	port(i_A : in std_logic_vector(N-1 downto 0);	--0
       	     i_B : in std_logic_vector(N-1 downto 0);	--1
             i_S : in std_logic;
             o_F : out std_logic_vector(N-1 downto 0));
  end component;

  component ALU_32_bit_Barrel_Shifter is
    port(i_A, i_B		: in std_logic_vector(31 downto 0);
         i_Op			: in std_logic_vector(2 downto 0);
	 i_S     		: in std_logic_vector(4 downto 0);
	 i_AorL 			: in std_logic;
	 i_RorL 			: in std_logic;
	 i_ALUorShifter 		: in std_logic;  --0 is ALU, 1 is Shifter
	 o_F			: out std_logic_vector(31 downto 0);
         o_Cout, o_OF, zero 	: out std_logic);
  end component;

  component n_bit_full_adder is
    generic(N : integer := 32);
    port(i_A	: in std_logic_vector(N-1 downto 0);
	 i_B	: in std_logic_vector(N-1 downto 0);
	 i_Cin	: in std_logic;
	 o_Cout	: out std_logic;
	 o_Cout_1: out std_logic;
	 o_Sum	: out std_logic_vector(N-1 downto 0));
  end component;

  component nbit_register is
    generic(N: integer := 32);
    port(i_CLK        : in std_logic;  
         i_RST        : in std_logic;
         i_WE         : in std_logic;    
         i_D          : in std_logic_vector(N-1 downto 0);   
         o_Q          : out std_logic_vector(N-1 downto 0));
  end component;

  component extenderComponents is
    generic(N: integer := 16);
    port(input 		: in std_logic_vector(15 downto 0);
	 i_Ctrl 	: in std_logic;
	 output 	: out std_logic_vector(31 downto 0));
  end component;

  component BarrelShifter
     generic(N : integer := 32);
     port(i_A    : in std_logic_vector(N-1 downto 0);
	  i_S    : in std_logic_vector(4 downto 0);
	  i_AorL : in std_logic;
	  i_RorL : in std_logic;
	  o_F    : out std_logic_vector(N-1 downto 0));
  end component;

  component ControlUnit is
    port(op_Code        									: in std_logic_vector(5 downto 0);
	  Funct	       										: in std_logic_vector(5 downto 0);
	  o_Op          									: out std_logic_vector(2 downto 0);
	  RegDst, o_beq, o_bne, Jump, jr, MemtoReg, MemWrite, ALUSrc, RegWrite 			: out std_logic;
	  o_Signed, o_AorL, o_RorL, o_ALUorShifter, o_ShiftVariable, o_upper_imm, o_sltu 	: out std_logic);
  end component;

  component PC_register is
    generic(N: integer := 32);
    port(i_CLK        : in std_logic;  
         i_RST        : in std_logic;
         i_WE         : in std_logic;    
         i_D          : in std_logic_vector(N-1 downto 0);   
         o_Q          : out std_logic_vector(N-1 downto 0));
  end component;

  signal s_ALUSrcD, s_imm_ext, s_rt, s_rd, s_ALUOut, s_upper_immD, s_jump_addr_top, s_PCOut, s_MemtoRegD, s_upper_imm_muxD, s_sltuD, s_jump_addr, s_jr_muxD, s_jump_muxD, s_sltu_muxD, s_branch_muxD, s_branch_add_out, s_imm_sl2	: std_logic_vector(31 downto 0);
  signal s_jump_temp, s_jump_addr_bottom																				: std_logic_vector(27 downto 0);
  signal s_ShiftBits, s_RegDst_muxD																					: std_logic_vector(4 downto 0);
  signal s_ALUOp																					: std_logic_vector(2 downto 0);
  signal s_RegDst, s_ALUSrc, s_Signed, s_AorL, s_RorL, s_ALUorShifter, s_zero, s_MemtoReg, s_Jump, s_ShiftVariable, s_upper_imm, s_sltu, s_bne, s_beq, s_Branch, s_jr			: std_logic;


begin

  -- TODO: This is required to be your final input to your instruction memory. This provides a feasible method to externally load the memory module which means that the synthesis tool must assume it knows nothing about the values stored in the instruction memory. If this is not included, much, if not all of the design is optimized out because the synthesis tool will believe the memory to be all zeros.
  with iInstLd select
    s_IMemAddr <= s_NextInstAddr when '0',
      iInstAddr when others;


  IMem: mem
    generic map(ADDR_WIDTH => 10,
                DATA_WIDTH => N)
    port map(clk  => iCLK,
             addr => s_IMemAddr(11 downto 2),
             data => iInstExt,
             we   => iInstLd,
             q    => s_Inst);
  
  DMem: mem
    generic map(ADDR_WIDTH => 10,
                DATA_WIDTH => N)
    port map(clk  => iCLK,
             addr => s_DMemAddr(11 downto 2),
             data => s_DMemData,
             we   => s_DMemWr,
             q    => s_DMemOut);

  s_Halt <='1' when (s_Inst(31 downto 26) = "000000") and (s_Inst(5 downto 0) = "001100") and (v0 = "00000000000000000000000000001010") else '0';

  -- TODO: Implement the rest of your processor below this comment! 

  reg : RegFile
    generic map(N => 32)
    port map(i_CLK => iCLK,
	     i_RST => iRST,
	     rt => s_Inst(25 downto 21),
	     rd => s_Inst(20 downto 16),
	     rw => s_RegWrAddr,
	     WE => s_RegWr,
	     Data => s_RegWrData,
	     o_2 => v0,
	     o_rt => s_rt,
	     o_rd => s_rd);

  s_DMemData <= s_rd;

  RegDst_mux : Nmux_dataflow
    generic map(N => 5)
    port map(i_A => s_Inst(20 downto 16),
	     i_B => s_Inst(15 downto 11),
	     i_S => s_RegDst,
	     o_F => s_RegDst_muxD);

  ALUSrc_mux : Nmux_dataflow
    generic map(N => 32)
    port map(i_A => s_rd,
	     i_B => s_imm_ext,
	     i_S => s_ALUSrc,
	     o_F => s_ALUSrcD);

  imm_extender : extenderComponents
    generic map(N => 16)
    port map(input => s_Inst(15 downto 0),
	     i_Ctrl => s_Signed,
	     output => s_imm_ext);

  ALU : ALU_32_bit_Barrel_Shifter
    port map(i_A => s_rt,
	     i_B => s_ALUSrcD,
	     i_Op => s_ALUOp,
	     i_S => s_ShiftBits,
	     i_AorL => s_AorL,
	     i_RorL => s_RorL,
	     i_ALUorShifter => s_ALUorShifter,
	     o_F => s_ALUOut,
	     o_Cout => open,
	     o_OF => open,
	     zero => s_zero);

  s_DMemAddr <= s_ALUOut;

  oALUOut <= s_ALUOut;

  MemtoReg_mux : Nmux_dataflow
    generic map(N => 32)
    port map(i_A => s_ALUOut,
	     i_B => s_DMemOut,
	     i_S => s_MemtoReg,
	     o_F => s_MemtoRegD);


  PC : PC_register
    generic map(N => 32)
    port map(i_CLK => iCLK,
	     i_RST => iRST,
	     i_WE => '1',
	     i_D => s_jr_muxD,
	     o_Q => s_PCOut);
  
  PC_increment : n_bit_full_adder
    generic map(N => 32)
    port map(i_A => s_PCOut,
	 i_B => x"00000004",
	 i_Cin => '0',
	 o_Cout => open,
	 o_Cout_1 => open,
	 o_Sum => s_jump_addr_top);

  s_NextInstAddr <= s_PCOut;

  control : ControlUnit
    port map(op_Code => s_Inst(31 downto 26),
	     Funct => s_Inst(5 downto 0),
	     o_Op => s_ALUOp,
	     RegDst => s_RegDst,
	     Jump => s_Jump,
	     MemtoReg => s_MemtoReg,
	     MemWrite => s_DMemWr,
	     ALUSrc => s_ALUSrc,
	     RegWrite => s_RegWr,
	     o_Signed => s_Signed,
	     o_AorL => s_AorL,
	     o_RorL => s_RorL,
	     o_ALUorShifter => s_ALUorShifter,
	     o_ShiftVariable => s_ShiftVariable,
	     o_upper_imm => s_upper_imm,
	     o_sltu => s_sltu,
	     o_bne => s_bne,
	     o_beq => s_beq,
	     jr => s_jr);

  ShiftVariable_mux : Nmux_dataflow
    generic map(N => 5)
    port map(i_A => s_Inst(10 downto 6),
	     i_B => s_rt(4 downto 0),
	     i_S => s_ShiftVariable,
	     o_F => s_ShiftBits);

  imm_shift : BarrelShifter
    port map(i_A => s_imm_ext,
	     i_S => "10000",
	     i_AorL => '1',
	     i_RorL => '1',
	     o_F => s_upper_immD);

  UpperImm_mux : Nmux_dataflow
    generic map(N => 32)
    port map(i_A => s_MemtoRegD,
	     i_B => s_upper_immD,
	     i_S => s_upper_imm,
	     o_F => s_upper_imm_muxD);

--  G1: for i in 1 to 31 generate
--        s_sltuD(i) <= '0';
--    end generate;

process (s_rt, s_ALUSrcD, s_ALUOut)
begin
  if ((s_rt(31) = '0') and (s_ALUSrcD(31) = '1')) then
	s_sltuD <= x"00000001";
  elsif ((s_rt(31) = '1') and (s_ALUSrcD(31) = '0')) then
	s_sltuD <= x"00000000";
  else
	s_sltuD <= s_ALUOut;
  end if;
end process;

  sltu_mux : Nmux_dataflow
    generic map(N => 32)
    port map(i_A => s_upper_imm_muxD,
	     i_B => s_sltuD,
	     i_S => s_sltu,
	     o_F => s_sltu_muxD);


--JUMP AND BRANCH LOGIC

  s_jump_temp(27 downto 26) <= "00";
  s_jump_temp(25 downto 0) <= s_Inst(25 downto 0);

  jump_addr_shift : BarrelShifter
    generic map(N => 28)
    port map(i_A => s_jump_temp,
	     i_S => "00010",
	     i_AorL => '1',
	     i_RorL => '1',
	     o_F => s_jump_addr_bottom);
  
--  jump_addr_offset : n_bit_full_adder
--    generic map(N => 28)
--    port map(i_A => s_jump_addr_top,
--	     i_B => x"0400000",
--	     i_Cin => '0',
--	     o_Cout => open,
--	     o_Cout_1 => open,
--	     o_Sum => s_return_addr);

  s_jump_addr(31 downto 28) <= s_jump_addr_top(31 downto 28);
  s_jump_addr(27 downto 0) <= s_jump_addr_bottom;

  jump_mux : Nmux_dataflow
    port map(i_A => s_branch_muxD,
	     i_B => s_jump_addr,
	     i_S => s_Jump,
	     o_F => s_jump_muxD);

  jr_mux : Nmux_dataflow
    port map(i_A => s_jump_muxD,
	     i_B => s_rt,
	     i_S => s_jr,
	     o_F => s_jr_muxD);

  branch_mux : Nmux_dataflow
    port map(i_A => s_jump_addr_top,
	     i_B => s_branch_add_out,
	     i_S => s_Branch,
	     o_F => s_branch_muxD);

  branch_add : n_bit_full_adder
    port map(i_A => s_jump_addr_top,
	     i_B => s_imm_sl2,
	     i_Cin => '0',
	     o_Cout => open,
	     o_Cout_1 => open,
	     o_Sum => s_branch_add_out);

  branch_shift : BarrelShifter
    generic map(N => 32)
    port map(i_A => s_imm_ext,
	     i_S => "00010",
	     i_AorL => '1',
	     i_RorL => '1',
	     o_F => s_imm_sl2);

  jal_write_mux : Nmux_dataflow
    port map(i_A => s_sltu_muxD,
	     i_B => s_jump_addr_top,
	     i_S => s_Jump,
	     o_F => s_RegWrData);

  jal_read_mux : Nmux_dataflow
    generic map(N => 5)
    port map(i_A => s_RegDst_muxD,
	     i_B => "11111",
	     i_S => s_Jump,
	     o_F => s_RegWrAddr);

  branch : process (s_beq, s_bne, s_zero)
  begin
    if ((s_zero = '1') and (s_beq = '1')) then
      s_Branch <= '1';
    elsif ((not (s_zero = '1')) and (s_bne = '1')) then
      s_Branch <= '1';
    else
      s_Branch <= '0';
    end if;
  end process;

  halt : process (v0, s_Halt)
  begin
    if (v0 = x"0000000A") then
      s_Halt <= '1';
    else 
      s_Halt <= '0';
    end if;
  end process;

end structure;
