-- Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------

-- extenderComponents.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the components 
-- required by a MIPS processor implementation
--
--
-- NOTES:
-- 9/17/2019 by TJN::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity extenderComponents is
   generic(N : integer := 16);
   port(input : in std_logic_vector(N-1 downto 0);
	i_Ctrl : in std_logic;
	output : out std_logic_vector(31 downto 0));
end extenderComponents;

architecture dataflow of extenderComponents is 

   begin
	G0 : for i in 0 to N-1 generate
	   output(i) <= input(i);
	end generate;

	G1 : for i in N to 31 generate
	   output(i) <= i_Ctrl AND input(N-1);
	end generate;

end dataflow;