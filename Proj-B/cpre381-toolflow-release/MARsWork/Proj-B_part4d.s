#
# Part 4d of the Lab 3 test program
#
# MARsWork/Proj-B_part4d.s

# data section
.data
	  
# code/instruction section
.text

# BRANCHES
not_branch_here:
	addi $1, $0, 1
	addi $2, $0, 2
	addi $3, $0, -3
	addi $4, $0, -4

	bne $1, $1, not_branch_here
	bne $1, $2, bne_here

	addi $15, $1, 1

bne_here:
	bne $3, $3, not_branch_here
	bne $3, $4, bne2_here

	addi $16, $1, 1

bne2_here:
	beq $1, $2, not_branch_here
	beq $1, $1, beq_here

	addi $17, $1, 1

beq_here:
	beq $3, $4, not_branch_here
	beq $3, $3, jump_start

	addi $18, $1, 1

# JUMPS

jump_start:
	addi $15, $1, 5
	add $16, $5, $6

	j jump_target

skip1:
	addi $12, $1, 5
	add $14, $5, $6

jump_target:
	addi $31, $1, 5
	add $19, $5, $6

	jal jal_target
	j exit

	addi $18, $1, 5
	add $15, $5, $6
	
skip2:
	addi $12, $1, 5
	add $14, $5, $6

jal_target:
	jr $31

exit:
	addi  $2,  $0,  10              # Place 10 in $v0 to signal a halt
	syscall