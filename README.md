# CprE 381

CprE 381 labs and projects

The complete final project code is located in the "Proj_C/cpre381-toolflow-release" folder: assembly code tests in the "MARsWork" folder, VHDL code in the "ModelSimWork" folder. The rest of the files are lab reports and synthesis scripts for testing.