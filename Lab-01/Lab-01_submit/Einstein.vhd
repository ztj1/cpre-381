library IEEE;
use IEEE.std_logic_1164.all;


entity Einstein is

  port(iCLK             	: in std_logic;
       m 		            : in integer;
       E 		            : out integer);

end Einstein;

architecture structure of Einstein is

  component Multiplier
    port(iCLK           : in std_logic;
         iA             : in integer;
         iB             : in integer;
         oC             : out integer);
  end component;

  -- Arbitrary constants for the A, B, C values. No need to change these.
  constant C : integer := 9487;

  -- Signals to store c^2
  signal c_square : integer;


begin

  
  ---------------------------------------------------------------------------
  -- Level 1: Calculate c^2
  ---------------------------------------------------------------------------
  mult1: Multiplier
    port MAP(iCLK             => iCLK,
             iA               => C,
             iB               => C,
             oC               => c_square);
    
 ---------------------------------------------------------------------------
  -- Level 2: Calculate m*(c^2)
  ---------------------------------------------------------------------------
  mult2: Multiplier
    port MAP(iCLK             => iCLK,
             iA               => c_square,
             iB               => m,
             oC               => E);
  
end structure;