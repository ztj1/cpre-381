-------------------------------------------------------------------------
-- Zach Johnson
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- n_bit_full_adder_TB.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: Testbench for the structural and dataflow implementations
-- 		of the n-bit full adder
--
--
-- NOTES:
-- 9/10/19 by ZTJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity n_bit_full_adder_TB is
generic(N : integer := 32);
end n_bit_full_adder_TB;

architecture behavior of n_bit_full_adder_TB is

component n_bit_full_adder
generic(N : integer := 32);
port(i_A, i_B	: in std_logic_vector(N-1 downto 0);
     i_Cin	: std_logic;
     o_Sum	: out std_logic_vector(N-1 downto 0);
     o_Cout	: out std_logic);
end component;

component n_bit_full_adder_df
generic(N : integer := 32);
port(i_A, i_B	: in std_logic_vector(N-1 downto 0);
     i_Cin	: std_logic;
     o_Sum	: out std_logic_vector(N-1 downto 0);
     o_Cout	: out std_logic);
end component;

signal s_Sum_ST, s_Sum_DF, s_A, s_B	: std_logic_vector(N-1 downto 0);
signal s_Cin, s_carry_ST, s_carry_DF	: std_logic;

begin

full_adder_st : n_bit_full_adder
port map(i_A => s_A,
	 i_B => s_B,
	 i_Cin => s_Cin,
	 o_Sum => s_Sum_ST,
	 o_Cout => s_carry_ST);

full_adder_df : n_bit_full_adder_df
port map(i_A => s_A,
	 i_B => s_B,
	 i_Cin => s_Cin,
	 o_Sum => s_Sum_DF,
	 o_Cout => s_carry_DF);

process
begin

s_A <= x"10101010";
s_B <= x"01010101";
s_Cin <= '0';
wait for 100 ns;

s_A <= x"10101010";
s_B <= x"01010101";
s_Cin <= '1';
wait for 100 ns;

s_A <= x"FAFAFAFA";
s_B <= x"00000000";
s_Cin <= '1';
wait for 100 ns;

s_A <= x"FAFAFAFA";
s_B <= x"05050505";
s_Cin <= '0';
wait for 100 ns;

s_A <= x"04040404";
s_B <= x"FBFBFBFB";
s_Cin <= '1';
wait for 100 ns;

end process;

end behavior;