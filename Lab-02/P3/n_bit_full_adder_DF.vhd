-------------------------------------------------------------------------
-- Zach Johnson
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- n_bit_full_adder_DF.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: A generic n-bit n:1 full adder using VHDL dataflow model
-- 
--
--
-- NOTES:
-- 9/10/19 by ZTJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity n_bit_full_adder_DF is

generic(N: integer := 32);
port(i_A, i_B	: in std_logic_vector(N-1 downto 0);
     i_Cin	: in std_logic;
     o_Sum	: out std_logic_vector(N-1 downto 0);
     o_Cout	: out std_logic);

end n_bit_full_adder_DF;

architecture dataflow of n_bit_full_adder_DF is

signal s_carry	: std_logic_vector(N-1 downto 0);

begin

G0:
o_Sum(0) <= i_A(0) xor i_B(0) xor i_Cin;
s_carry(0) <= (i_A(0) and i_B(0)) or (i_Cin and (i_A(0) xor i_B(0)));

G1: for i in 1 to N-2 generate
o_Sum(i) <= i_A(i) xor i_B(i) xor s_carry(i-1);
s_carry(i) <= (i_A(i) and i_B(i)) or (s_carry(i-1) and (i_A(i) xor i_B(i)));
end generate;

G2:
o_Sum(N-1) <= i_A(N-1) xor i_B(N-1) xor s_carry(N-2);
o_Cout <= (i_A(N-1) and i_B(N-1)) or (s_carry(N-2) and (i_A(N-1) xor i_B(N-1)));

end dataflow;
