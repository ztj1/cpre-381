-------------------------------------------------------------------------
-- Zach Johnson
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- add_sub_TB.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: A testbench for the n-bit adder-subtractor
-- 
--
--
-- NOTES:
-- 9/10/19 by ZTJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity add_sub_TB is
generic(N : integer := 32);
end add_sub_TB;

architecture behavior of add_sub_TB is

component add_sub
generic(N : integer := 32);
port(i_A	: in std_logic_vector(N-1 downto 0);
     i_B	: in std_logic_vector(N-1 downto 0);
     nAdd_Sub	: in std_logic;
     o_Sum	: out std_logic_vector(N-1 downto 0);
     o_Cout	: out std_logic);
end component;

signal s_A, s_B, s_Sum	: std_logic_vector(N-1 downto 0);
signal s_Cout, s_Add_Sub		: std_logic;

begin

add_sub_1 : add_sub
port map(i_A => s_A,
	 i_B => s_B,
	 o_Cout => s_Cout,
	 o_Sum => s_Sum,
	 nAdd_Sub => s_Add_Sub);

process
begin

s_A <= x"FFFFFFEE";	--show that addition works up to max value without carry out
s_B <= x"00000011";
s_Add_Sub <='0';
wait for 100 ns;

s_A <= x"00000000";	--show that addition works with empty values
s_B <= x"00000000";
s_Add_Sub <= '0';
wait for 100 ns;

s_A <= x"FFFFFFEE";	--show that addition works with carry in and carry out
s_B <= x"00000011";
s_Add_Sub <= '0';
wait for 100 ns;

s_A <= x"FAFAFAFA";	--prove that carry out actually works and isn't just equal to the carry in
s_B <= x"05050506";
s_Add_Sub <= '0';
wait for 100 ns;

s_A <= x"FFFFFFFF";	--show that subtraction works
s_B <= x"55555555";
s_Add_Sub <= '1';
wait for 100 ns;

s_A <= x"04040404";	--show that subtraction with carry in works
s_B <= x"FBFBFBFB";
s_Add_Sub <= '1';
wait for 100 ns;

s_A <= x"04040404";	
s_B <= x"FBFBFBFB";
s_Add_Sub <= '1';
wait for 100 ns;


end process;

end behavior;