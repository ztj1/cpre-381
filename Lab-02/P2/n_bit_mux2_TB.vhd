-------------------------------------------------------------------------
-- Zach Johnson
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- n_bit_mux2_TB.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: Testbench for the structural and dataflow implementations
-- 		of the n-bit mux
--
--
-- NOTES:
-- 9/10/19 by ZTJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity n_bit_mux2_TB is
generic(N : integer := 32);
end n_bit_mux2_TB;

architecture behavior of n_bit_mux2_TB is

component n_bit_mux2
generic(N : integer := 32);
port(i_A, i_B	: in std_logic_vector(N-1 downto 0);
     i_S	: std_logic;
     o_F	: out std_logic_vector(N-1 downto 0));
end component;

component n_bit_mux2_df
generic(N : integer := 32);
port(i_A, i_B	: in std_logic_vector(N-1 downto 0);
     i_S	: std_logic;
     o_F	: out std_logic_vector(N-1 downto 0));
end component;

signal s_F_st, s_F_df, s_A, s_B	: std_logic_vector(N-1 downto 0);
signal s_S	: std_logic;

begin

mux_st : n_bit_mux2
port map(i_A => s_A,
	 i_B => s_B,
	 i_S => s_S,
	 o_F => s_F_st);

mux_df : n_bit_mux2_df
port map(i_A => s_A,
	 i_B => s_B,
	 i_S => s_S,
	 o_F => s_F_df);

process
begin

s_A <= x"ABCD0000";
s_B <= x"0000ABCD";
s_S <= '1';
wait for 100 ns;

s_A <= x"00000000";
s_B <= x"FBFBFBFB";
s_S <= '1';
wait for 100 ns;

s_A <= x"FAFAFAFA";
s_B <= x"00000000";
s_S <= '1';
wait for 100 ns;

s_A <= x"FAFAFAFA";
s_B <= x"00000000";
s_S <= '0';
wait for 100 ns;

s_A <= x"00000000";
s_B <= x"FBFBFBFB";
s_S <= '0';
wait for 100 ns;

end process;

end behavior;