-------------------------------------------------------------------------
-- Zach Johnson
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- n_bit_mux2_DF.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: A generic n-bit n:1 mux using VHDL dataflow model
-- 
--
--
-- NOTES:
-- 9/9/19 by ZTJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity n_bit_mux2_DF is
generic(N : integer := 32);
port(i_A, i_B  : in std_logic_vector(N-1 downto 0);
     i_S : std_logic;
     o_F  : out std_logic_vector(N-1 downto 0));
end n_bit_mux2_DF;

architecture dataflow of n_bit_mux2_DF is

begin

o_F <= i_A when (i_S = '0') else i_B;
  
end dataflow;