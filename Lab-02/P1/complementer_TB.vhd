-------------------------------------------------------------------------
-- Zach Johnson
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- complementer_TB.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: Testbench for the n-bit complementer structural and dataflow implementations
--
--
-- NOTES:
-- 9/10/19 by ZTJ::Design created.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity complementer_TB is
generic(N : integer := 32);
end complementer_TB;

architecture behavior of complementer_TB is

component complementer
generic(N : integer := 32);
port(i_A	: in std_logic_vector(N-1 downto 0);
     o_F	: out std_logic_vector(N-1 downto 0));
end component;

component complementer_DF
generic(N : integer := 32);
port(
      i_A	: in std_logic_vector(N-1 downto 0);
      o_F	: out std_logic_vector(N-1 downto 0));
end component;

signal s_A, s_F	: std_logic_vector(N-1 downto 0);

begin

complementer_st_1 : complementer
port map(i_A => s_A,
	 o_F => s_F);

complementer_df_1 : complementer_DF
port map(i_A => s_A,
	 o_F => s_F);

process
begin

s_A <= x"10101010";
wait for 100 ns;

s_A <= x"10101010";
wait for 100 ns;

s_A <= x"FAFAFAFA";
wait for 100 ns;

s_A <= x"FAFAFAFA";
wait for 100 ns;

s_A <= x"04040404";
wait for 100 ns;

end process;

end behavior;