library IEEE;
use IEEE.std_logic_1164.all;

entity complementer_DF is

   generic(N : integer := 32);
   port(
      i_A	: in std_logic_vector(N-1 downto 0);
      o_F	: out std_logic_vector(N-1 downto 0));

end complementer_DF;

architecture dataflow of complementer_DF is

begin

    o_F <= not i_A;

end dataflow;